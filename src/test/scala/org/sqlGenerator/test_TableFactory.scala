package org.sqlGenerator

import org.common.Globals
import org.mavok.api.table.{AbstractTable, TableGenericFactory, TablePath}
import org.mavok.datatype.dbtype._
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.Table6Columns

class test_TableFactory extends FlatSpec with Matchers {


  "Table Factory" should "get column names and types using table name from DB Meta" in {


    val table: AbstractTable = TableGenericFactory( TablePath( "ctrl_kafka.ctrl_kafka_datastore" ), Globals.TEST_POSTGRES_CONNECTION )

    require( table.getColumns.nonEmpty )

  }

  it should "build Mavok Table object matching DB Table DDL" in {


    assert( 1 == 0 )
  }


  it should "read in columns correctly from table model" in {

    if( Table6Columns.controller.tableExists ) Table6Columns.controller.drop
    Table6Columns.controller.create

    val table = TableGenericFactory( TablePath( Table6Columns.tablePath.getTablePath ), Table6Columns.controller.jdbcPlugin )

    val origCols = ( Table6Columns.getColumns.sortBy( _.name.name ).map( _.name.name.toLowerCase() ) zip Table6Columns.getColumns.sortBy( _.name.name ).map( _.dbType.toDDL.toSQL( Table6Columns.controller.jdbcPlugin.getConnectionType).map( _.value ).mkString  ) )
    val derivedCols = ( table.getColumns.sortBy( _.name.name ).map( _.name.name.toLowerCase() ) zip table.getColumns.sortBy( _.name.name ).map( _.dbType.toDDL.toSQL( Table6Columns.controller.jdbcPlugin.getConnectionType).map( _.value ).mkString  ) )

    require( origCols == derivedCols, s"-- Original\n ${origCols.map( col => col._1 + " " + col._2 ) }" +
      s"-- Derive\n ${derivedCols.map( col => col._1 + " " + col._2)}" )

  }


  it should "read in primitive types correctly from table model" in {

    val expectedResults = Vector( PrimitiveType.LONG, PrimitiveType.STRING, PrimitiveType.STRING, PrimitiveType.STRING, PrimitiveType.DATE,
      PrimitiveType.BIGINT, PrimitiveType.LONG, PrimitiveType.TIMESTAMP, PrimitiveType.FLOAT, PrimitiveType.DOUBLE )

    if( Table6Columns.controller.tableExists ) { Table6Columns.controller.drop }
    Table6Columns.controller.create

    val table = TableGenericFactory( TablePath( Table6Columns.tablePath.getTablePath ), Table6Columns.controller.jdbcPlugin )

    val origCols = ( Table6Columns.getColumns.sortBy( _.name.name ).map( _.dbType.typePattern.primitiveType ) )
    val derivedCols = ( table.getColumns.sortBy( _.name.name ).map( _.dbType.typePattern.primitiveType ) )

    require(  origCols== derivedCols, s"-- Original\n ${origCols.mkString }" +
      s"-- Derive\n ${derivedCols.mkString}" )



    require( expectedResults == derivedCols, s"-- Original\n ${origCols.mkString }" +
       s"-- Derived \n ${derivedCols.mkString } \n" +
      s"-- Expected \n ${expectedResults.mkString}" )

  }




}
