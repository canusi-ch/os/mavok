package org.sqlGenerator

import org.mavok.common.ConnectionType
import org.mavok.jdbc.JDBCPlugin


case class ConfigName( value: String ) extends AnyVal

object JDBCConnection {


  def apply(  _connectionType: ConnectionType.Value, _configName: ConfigName  ): JDBCConnection = {

    new JDBCConnection {
      val connectionType: ConnectionType.Value = _connectionType
      override val configName: String = _configName.value
    }
  }



}



trait JDBCConnection  extends JDBCPlugin {


}
