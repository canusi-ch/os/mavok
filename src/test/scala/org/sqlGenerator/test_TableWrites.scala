package org.sqlGenerator

import org.common.Globals
import org.mavok.LOG
import org.mavok.api.sql.DMLBuilder
import org.mavok.datatype.dbtype.{BIGINT, DBColumnType}
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.{Table2Columns, Table3Columns}

class test_TableWrites  extends FlatSpec with Matchers {


  "DML Engine" should "generate sql for update values" in {

    import org.mavok.api.Implicits._
    val sqlStatement = DMLBuilder( Table2Columns )
      .updateValues( Table2Columns.colInt << "Test", Table2Columns.colInt << "Test")
      .on( Table2Columns.colInt :== "0")
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)

  }


  it should "generate sql for inner join" in {

    val sqlStatement = DMLBuilder( Table2Columns )
      .select( Table2Columns.colInt,  Table2Columns.colString.as[BIGINT](DBColumnType.BIGINT) )
      .inner( Table3Columns )
      .on( ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colString :== Table2Columns.colString )
        && ( Table3Columns.colInt :== Table2Columns.colInt )
        && ( Table3Columns.colString :== Table2Columns.colString )
      )
      .where( Table3Columns.colString isNull )
      .build( Globals.TEST_POSTGRES_CONNECTION.getConnectionType )

    LOG.info( "Finished test build", sqlStatement)
  }





}
