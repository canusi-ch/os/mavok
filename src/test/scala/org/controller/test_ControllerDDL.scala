package org.controller

import org.mavok.api.table.{TableGenericFactory, TableModel, TablePath}
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.{TableModelRecord, TableModelTest}
import testEngine.TableBuilder

class test_ControllerDDL extends FlatSpec with Matchers with TableBuilder{

  override val tableList: Vector[TableModel[_ <: Product]] = Vector( TableModelTest )

  "Controller" should "create table with PKs" in {
    if( TableModelTest.controller.tableExists ) TableModelTest.controller.drop
    TableModelTest.controller.create

    val readTableBack = TableGenericFactory(
      TablePath( TableModelTest.tablePath.getTablePath )
      , TableModelTest.controller.jdbcPlugin)

    require( readTableBack.getColumns.forall( column => column.isPK == TableModelTest.getColumns.filter( _.name.name.toLowerCase() == column.name.name.toLowerCase() ).head.isPK ) )
  }


  it should "create table with PKs, which factory can read in correctly" in {
    val nextRecord = TableModelRecord( 2, "test", "test" )
    TableModelTest.controller += nextRecord
    val results = TableModelTest.controller.filter( _.colPkId :== "2" )
    require( results.size == 1 )
    require( results.head == nextRecord )
  }

}
