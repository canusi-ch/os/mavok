package org.controller

import java.util.UUID

import org.apache.avro.generic.GenericData
import org.common.Globals
import org.mavok.api.table.{TableGenericFactory, TableModel, TablePath}
import org.mavok.common.classvalue.StringAvroDefinition
import org.mavok.datatype.Avro2DBColumnMapping
import org.scalatest.{FlatSpec, Matchers}
import org.testModels.TableModelTest
import testEngine.TableBuilder

import scala.io.Source

class test_ControllerGeneric extends FlatSpec with Matchers with TableBuilder{

  override val tableList: Vector[TableModel[_ <: Product]] = Vector( TableModelTest )

  "Controller" should "create table from Avro Definition" in {
    val jsonFile = Source.fromResource("jsonFiles/StringAvroTableDefinitionGeneric.json")

    val avroString = jsonFile.getLines().mkString
    val avroSchema = Avro2DBColumnMapping( StringAvroDefinition( avroString ) )
    val tablePath = TablePath( avroSchema.name.name, UUID.randomUUID() )
    val table = TableGenericFactory.fromAvro( avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION  )

    if( table.controller.tableExists ) table.controller.drop
    table.controller.create

  }


  it should "insert value from Avro Definition" in {
    val jsonFile = Source.fromResource("jsonFiles/StringAvroTableDefinitionGeneric.json")

    val avroString = jsonFile.getLines().mkString
    val avroSchema = Avro2DBColumnMapping( StringAvroDefinition( avroString ) )
    val tablePath = TablePath( avroSchema.name.name, UUID.randomUUID() )
    val table = TableGenericFactory.fromAvro( avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION  )

    if( table.controller.tableExists ) table.controller.drop
    table.controller.create

    val avroRecord = new GenericData.Record(table.avroSchema.schema)
    avroRecord.put( "testdecimal", 25.4 )
    avroRecord.put( "testvarchar", "testing")
    avroRecord.put( "testint", 32 )

    table.controller.+=( avroRecord )

  }



  it should "update value from Avro Definition" in {
    val jsonFile = Source.fromResource("jsonFiles/StringAvroTableDefinitionGeneric.json")

    val avroString = jsonFile.getLines().mkString
    val avroSchema = Avro2DBColumnMapping( StringAvroDefinition( avroString ) )
    val tablePath = TablePath( avroSchema.name.name, UUID.randomUUID() )
    val table = TableGenericFactory.fromAvro( avroSchema, tablePath, Globals.TEST_POSTGRES_CONNECTION  )

    if( table.controller.tableExists ) table.controller.drop
    table.controller.create

    val avroRecord = new GenericData.Record(table.avroSchema.schema)
    avroRecord.put( "testdecimal", 25.4 )
    avroRecord.put( "testvarchar", "testing")
    avroRecord.put( "testint", 32 )

    table.getColumns.filter( _.name.name == "testint" ).head.asPK

    table.controller.update( avroRecord )

  }




}
