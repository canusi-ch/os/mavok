package org.testModels

import org.common.Globals
import org.mavok.api.table.{TableModel, TablePath}
import org.mavok.datatype.dbtype.STRING

case class TableModelRecord( pkId: Int, orange: String, apple: String )


object TableModelTest extends TableModel[TableModelRecord](
    TablePath( Globals.TEST_SCHEMA, "tablemodeltest")
  , Globals.TEST_POSTGRES_CONNECTION ) {

  val colPkId = column[STRING]( "pkId" ).asPK
  val colOrange = column[STRING]( "orange" )
  val colApple = column[STRING]( "apple" )
}
