package org.testModels

import org.common.Globals
import org.mavok.api.table.{TableModel, TablePath}
import org.mavok.datatype.dbtype.STRING

object Table2Columns extends TableModel( TablePath( Globals.TEST_SCHEMA, "test2Columns"), Globals.TEST_POSTGRES_CONNECTION ){


  val colString = column[STRING]( "stringColumn" ).asPK
  val colInt = column[STRING]( "intColumn" )

}
