package org.testModels

import org.common.Globals
import org.mavok.api.table.{TableModel, TablePath}
import org.mavok.datatype.dbtype._

case class Nothing()


object Table6Columns extends TableModel[Nothing]( TablePath( Globals.TEST_SCHEMA, "orange"), Globals.TEST_POSTGRES_CONNECTION  ){

  val colString2 = column[LONG]( "orange" )
  val colString = column[STRING]( "testing", DBColumnType.VARCHAR(255 ) )
  val colInt = column[STRING]( "apple" )
  val colTestMissing = column[STRING]( "string" )
  val colDate = column[DATE]( "colDate" )
  val colBigint = column[BIGINT]( "colBigInt" )
  val colLong = column[LONG]( "colLong" )
  val getTimestamp = column[TIMESTAMP]( "colTimestamp" )
  val getFloat = column[FLOAT]( "colFloat" )
  val getDouble = column[DOUBLE]( "colDouble" )


}


