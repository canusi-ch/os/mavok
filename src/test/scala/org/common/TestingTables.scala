package org.common

import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.VirtualTable
import org.mavok.common.ConnectionType
import org.mavok.datatype.dbtype.{DBColumnType, STRING}
import org.testModels.{Table2Columns, Table3Columns}

object TestingTables {



  object testingTableThreeVirtual extends VirtualTable{

    val colString = value[STRING]( "orange" )
    val colInt = value[STRING]( "apple" )
    val colTestMissing = value[STRING]( "missing" )

  }




  def main( string: Array[String] ):Unit = {



    println( Table2Columns.asShape.columns.mkString("|"))


    val connection = ConnectionType.ORACLE
    println(

      DMLBuilder( Table3Columns )
        .select( Table3Columns.colString ,Table3Columns.colInt ,  Table3Columns.colTestMissing )
        .build( connection )


    )

    println(

      DMLBuilder( Table3Columns )
        .select( Table3Columns.colString , Table3Columns.colInt , Table3Columns.colTestMissing )
        .build( connection )
    )



    import org.mavok.api.Implicits._

    println(


      DMLBuilder( Table3Columns )
        .select(   Table3Columns.colString , Table3Columns.colString.as[STRING]( DBColumnType.VARCHAR( 255 )) , convertStringToColumn( "" )  )
        .insert( Table3Columns )(  Table3Columns.colString , Table3Columns.colInt )
        .build( connection )

    )






    println(

      DMLBuilder( testingTableThreeVirtual )
        .select(  Table3Columns.colString , Table3Columns.colString.as[STRING](DBColumnType.VARCHAR( 255 )) )
        .insert( Table3Columns )(  Table3Columns.colInt , Table3Columns.colString)
        .build( connection )


    )




    println(

      DMLBuilder( Table3Columns )
        .select(  Table3Columns.colString , Table3Columns.colInt , Table3Columns.colTestMissing )
        .where(  ( Table3Columns.colString.as[STRING](DBColumnType.VARCHAR( 255 )) :== Table3Columns.colTestMissing )
          && ( Table3Columns.colString :== Table3Columns.colTestMissing )
          && ( ( Table3Columns.colString :== Table3Columns.colTestMissing )
          || ( Table3Columns.colString :== Table3Columns.colTestMissing )
          )
        )
        .build( connection )


    )
    println(

      DMLBuilder( Table3Columns )
        .select(  Table3Columns.colString , Table3Columns.colString.as[STRING](DBColumnType.VARCHAR( 255 )) )
        .update( Table2Columns )(  ( Table2Columns.colInt),  (  Table2Columns.colInt) )
        .on(  Table2Columns.colInt :==  Table3Columns.colString )
        .build( connection )

    )




    println(

      DMLBuilder( Table3Columns )
        .select(  Table3Columns.colString, Table3Columns.colString.as[STRING](DBColumnType.VARCHAR( 255 )) )
        .left( Table2Columns )
        .on( ( ( Table3Columns.colString :== Table2Columns.colInt )
          && ( Table3Columns.colInt :== "0" )
          ))
        .build( connection )
    )


    println(

      DMLBuilder( Table2Columns )
        .select( Table2Columns.colString concat Table2Columns.colInt )
        .update( Table2Columns ) ( Table2Columns.colString )
        .on( Table2Columns.colInt :== Table2Columns.colString )
        .build( connection )




    )







    /*





    println(

      DMLBuilder( testingTableThree )
        .select(
          testingTableThree.colString
          , testingTableThree.colInt
        )
        .update( testingTable.colString, testingTable.colInt )
        .on( testingTable.colInt :== testingTableThree.colString )
        .build( connection )
        .sql
    )





    println(

      DDLBuilder( testingTableThree )
        .create
        .build( connection )
        .sql
    )


    println(

      DDLBuilder( testingTableThree )
        .drop
        .build( connection )
        .sql
    )




    println(

      DDLBuilder( testingTableThree )
        .truncate
        .build( connection )
        .sql
    )







        insert( string.as[DATE] -> target )
        DMLBuilder( testingTable )
          .select( columns )
            .where( )


        DMLBuilder( testingTable )
          .select( testingTable.systemColumns.getChangeIndicator )





        DMLBuilder( testingTable )
          .insert( columns )
            .where( columns )


        DMLBuilder( testingTable )
          .update( columns )
            .where( columns )

        columns.cast
        columns.coalesce


        SQLDesignPatternFactory
        -getInsertColumnsOnColumnNames( table1, table2 )
        -getComparisonOnPrimaryKeys( table1, table2 )



        println(
          DDLBuilder( testingTable )
            .create
            .build( ConnectionType.ORACLE  )
            .sql
        )
        println(
          DDLBuilder( testingTable )
            .truncate
            .build( ConnectionType.ORACLE )
            .sql
        )


        3 join types
        group by
        order by
        filter

        coalesce
        cast

        lead/lag/coalesce


        select parallel


        DML( table )
          .build( ConnectionType.ORACLE, executionParams )


    -*/


  }




}
