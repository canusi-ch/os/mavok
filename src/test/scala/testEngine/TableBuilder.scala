package testEngine

import org.mavok.api.table.TableModel
import org.scalatest.{BeforeAndAfterAll, Suite}

import scala.util.{Failure, Success, Try}

trait TableBuilder extends BeforeAndAfterAll { this: Suite =>

  val tableList: Vector[TableModel[ _ <: Product]]

  override def beforeAll() {
    val t = Try( tableList.foreach{
                table => if( table.controller.tableExists ) { table.controller.drop }
                table.controller.create
    } )

    t match {
      case Success(value) => {}
      case Failure(exception) => throw exception
    }

    super.beforeAll() // To be stackable, must call super.beforeEach
  }

  override def afterAll() {

    val t = Try( tableList.foreach{
      table => if( table.controller.tableExists ) { table.controller.drop }
    } )

    t match {
      case Success(value) => {}
      case Failure(exception) => throw exception
    }

  }
}

