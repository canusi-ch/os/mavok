package org.mavok.scope

import org.mavok.resource.Resource
import org.mavok.shapeLibrary.rdms.common.ScopeResourceManager
import org.mavok.shapeLibrary.rdms.keywords.Alias

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 04.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object Scope {

  // ---------------------  Constructors

  def apply(): Scope = {
    new Scope()
  }

}

class Scope {

  private val scopeMap: mutable.HashMap[ScopeResourceManager, mutable.HashMap[Resource, Alias]] = mutable.HashMap[ScopeResourceManager, mutable.HashMap[Resource, Alias]]()


  private val listGlobalAliasResourceManagers: ListBuffer[ScopeResourceManager] = ListBuffer[ScopeResourceManager]()

  /**
    * shapes that are introduced within a given query will appear ( SELECT 1 FROM mytable -> mytable will be added to this list
    * Usage is to provide a way for queries to know whether they should be using the actual names of columns, or setting up an alias
    */
  private val shapesOwned: mutable.HashMap[ScopeResourceManager, ListBuffer[Resource]] = mutable.HashMap[ScopeResourceManager, ListBuffer[Resource]]()


  def getAlias(graph: ScopeResourceManager, elem: Resource ): Alias = {

    if( !scopeMap.contains( graph ) ) scopeMap( graph ) = mutable.HashMap[Resource, Alias]()
    if( !scopeMap( graph ).contains( elem ) ) scopeMap( graph )( elem ) = Alias()

    scopeMap( graph )( elem )
  }

  // todo this needs a rework
  def addGlobalAliasResourceManager( aliasResourceManager: ScopeResourceManager): Unit = {
    require( !listGlobalAliasResourceManagers.contains( aliasResourceManager ), "Not allowed to add ARM 2x" )
    listGlobalAliasResourceManagers += aliasResourceManager
  }



  // todo like global alias this needs a rewwork on the design .
  // Scope management should be modularized on a class basis
  def addOwnedShape(graph: ScopeResourceManager, elem: Resource ): Unit = {
    if( !shapesOwned.contains( graph ) ) shapesOwned( graph ) = ListBuffer[Resource]()
    if( !shapesOwned( graph ).contains( elem ) ) shapesOwned( graph ) += elem
  }


  def getOwnedResourceList(aliasResourceManager: ScopeResourceManager ) = {
    val listValues = shapesOwned.filter( _._1 == aliasResourceManager ).values
    if( listValues.size == 1) listValues.head
    else if( listValues.size > 1 )
      throw new IllegalArgumentException( "Should not be possible to reach here")
    else ListBuffer[Resource]()
  }



  def getGlobalAliasResourceManager = listGlobalAliasResourceManagers

}
