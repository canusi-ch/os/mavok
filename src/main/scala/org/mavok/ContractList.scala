package org.mavok

import org.mavok.common.ConnectionType

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait ContractList[T] {

  val defaultSchematic: T
  val schematicOracle12: T
  val schematicPostgres10: T


  final def getSchematicContract(connectionType: ConnectionType.Value ): T = {
    val schematicShape = connectionType match {
      case ConnectionType.POSTGRES => schematicPostgres10
      case ConnectionType.ORACLE => schematicOracle12

    }

    schematicShape
  }


}
