package org.mavok.meta

import org.mavok.ContractList
import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table._
import org.mavok.controller.ReadableTableController
import org.mavok.datatype.dbtype.{INTEGER, STRING}
import org.mavok.jdbc.JDBCPlugin
import org.mavok.workflowEngine.QueryFlow

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object MetaTable extends QueryModelGeneric {
  override val tableQuery: QueryFlow[_] = null
}





case class CaseClassMetaTableColumns( tablePath: String, columnName: String, columnTypeName: String, length: Int, numericWidth: Int, numericScale: Int )

trait MetaTableColumnInterface extends QueryModelGeneric {

  val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]]
  val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]]
  val colColumnTypeName: AbstractColumn[STRING,_ <: DatasetReadable[_]]
  val colLength: AbstractColumn[INTEGER,_ <: DatasetReadable[_]]
  val colNumericWidth: AbstractColumn[INTEGER,_ <: DatasetReadable[_]]
  val colNumericScale: AbstractColumn[INTEGER,_ <: DatasetReadable[_]]

}


private object PostgresAllTables extends AbstractTable{
  override val tablePath: HasTablePath = TablePath( "information_schema", "columns" )

  val colSchemaName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "table_schema")
  val colTableName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "table_name")
  val colColumnName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "column_name")
  val colColumnTypeName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "data_type")
  val colLength: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "character_maximum_length")
  val colNumericWidth: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "numeric_precision")
  val colNumericScale: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "numeric_scale")

}

object PostgresColumnInterface extends MetaTableColumnInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef[STRING]( PostgresAllTables.colSchemaName concat "."  concat( PostgresAllTables.colTableName ) )
  override val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef[STRING]( PostgresAllTables.colColumnName )
  override val colColumnTypeName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef[STRING]( PostgresAllTables.colColumnTypeName)
  override val colLength: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER]( PostgresAllTables.colLength )
  override val colNumericWidth: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER](  PostgresAllTables.colNumericWidth )
  override val colNumericScale: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER](  PostgresAllTables.colNumericScale )

  override def tableQuery: QueryFlow[_] = DMLBuilder( PostgresAllTables ).select( PostgresColumnInterface.getColumns: _* )


}

private object OracleAllTables extends AbstractTable{
  override val tablePath: HasTablePath = TablePath( "ALL_TABLES" )

  val colSchemaName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "OWNER")
  val colTableName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "TABLE_NAME")
  val colColumnName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "COLUMN_NAME")
  val colColumnTypeName: Column[STRING, _ <: DatasetReadable[_]] = column[STRING]( "COLUMN_TYPE_NAME")
  val colLength: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "character_length")
  val colNumericWidth: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "numeric_width")
  val colNumericScale: Column[INTEGER, _ <: DatasetReadable[_]] = column[INTEGER]( "numeric_scale")


}

object OracleColumnInterface extends MetaTableColumnInterface {

  import org.mavok.api.Implicits._
  override val colTablePath: AbstractColumn[STRING,_ <: DatasetReadable[_]] =  columnRef[STRING]( OracleAllTables.colSchemaName concat "."  concat( OracleAllTables.colTableName ) )
  override val colColumnName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef[STRING]( OracleAllTables.colColumnName )
  override val colColumnTypeName: AbstractColumn[STRING,_ <: DatasetReadable[_]] = columnRef[STRING]( OracleAllTables.colColumnTypeName)
  override val colLength: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER]( OracleAllTables.colLength )
  override val colNumericWidth: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER](  OracleAllTables.colNumericWidth )
  override val colNumericScale: AbstractColumn[INTEGER,_ <: DatasetReadable[_]] = columnRef[INTEGER](  OracleAllTables.colNumericScale )

  override def tableQuery: QueryFlow[_] = DMLBuilder( OracleAllTables ).select( OracleColumnInterface.getColumns: _*   )

}


object  MetaTableColumns extends ContractList[MetaTableColumnInterface] {
  override val defaultSchematic: MetaTableColumnInterface = PostgresColumnInterface
  override val schematicOracle12: MetaTableColumnInterface = OracleColumnInterface
  override val schematicPostgres10: MetaTableColumnInterface = PostgresColumnInterface

  def apply( _jdbcPlugin: JDBCPlugin): ReadableTableController[CaseClassMetaTableColumns, MetaTableColumnInterface] = {

    require( PostgresColumnInterface.getColumns.nonEmpty )

    val _tableQuery = getSchematicContract( _jdbcPlugin.getConnectionType )

    new ReadableTableController[CaseClassMetaTableColumns, MetaTableColumnInterface](){
      override val jdbcPlugin: JDBCPlugin = _jdbcPlugin
      override val table: MetaTableColumnInterface = _tableQuery
    }
  }

}





