package org.mavok.shapeLibrary.dbcontract

import org.mavok.ContractList
import org.mavok.common.ConnectionType
import org.mavok.shapeLibrary.rdms.common.GraphDBSchematicShape

/*
 * Copyright: blacktest -- created 22.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


// todo contracts needs to be allowed to be for a single database implementation
// this needs some design refactoring
trait DBSchematicContract extends ContractList[GraphDBSchematicShape]{

  override val defaultSchematic: GraphDBSchematicShape
  override val schematicOracle12: GraphDBSchematicShape
  override val schematicPostgres10: GraphDBSchematicShape

}
