package org.mavok.shapeLibrary.rdms.column.common

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.Alias

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

object ColumnAlias{


  def apply( alias: Alias ): ColumnAlias = {
    new ColumnAlias( alias )
  }
}



class ColumnAlias( alias: Alias ) extends ColumnGraphDefault {

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}

    override def schematic(): Schematic = Schematic().withKnown( Schematic.ONE, alias )
  }

}





