package org.mavok.shapeLibrary.rdms.column.dataType.conversions

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.dataType.AbstractDDLType
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.rdms.keywords.SQLColumnExpression

/*
 * Copyright: blacktest -- created 29.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class DDLTimestamp extends AbstractDDLType {

  override def width: Int = ???


  import org.mavok.common.schematic.Schematic._
  override val schematicOracle12: GraphDBSchematicShape = new GraphDBSchematicShape {

    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "TIMESTAMP") )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }


  override val schematicPostgres10: GraphDBSchematicShape = new GraphDBSchematicShape {

    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "TIMESTAMP") )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "TIMESTAMP") )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



}


object DDLTimestamp {
  def apply(): DDLTimestamp = new DDLTimestamp(){}
}

