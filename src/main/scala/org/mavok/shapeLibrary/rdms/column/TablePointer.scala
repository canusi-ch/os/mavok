package org.mavok.shapeLibrary.rdms.column

import org.mavok.LOG
import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.rdms.keywords.{Alias, PERIOD}

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object TablePointer {
  def apply( column: Column[_, _]): TablePointer = new TablePointer(column)
}

final class TablePointer(column: AbstractColumn[_, _] ) extends Graph with DBSchematicContractCommon {


  override val defaultSchematic: GraphDBSchematicShape  = new GraphDBSchematicShape {

    override def schematic(): Schematic =
      Schematic()
        .withFuture[Alias]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant find column for SQL header"))
        .withFuture[PERIOD.type]( Schematic.ONE, () => PERIOD )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit =    {

      val listArmSources = params.getChildAliasManagerWithResouce( column )
      val listGlobalSources = params.getGlobalScopeManagerWithResouce( column )
      if( listArmSources.nonEmpty){
        addShapeTo2ndBuilder__BuildFromContext( params.getAlias( listArmSources.head), params )
      }
      else if( listGlobalSources.nonEmpty ){
        addShapeTo2ndBuilder__BuildFromContext( listGlobalSources.head.getAlias(  listGlobalSources.head.getChildAliasManagerWithResouce( column ).head  ), params )
      } else{
        LOG.error( "No ARM sources found", s"${this} has no child or global arms in ${params}", () =>
          throw new IllegalArgumentException( s"${this} has no child or global arms in ${params}" ))
      }
    }

  }


}







