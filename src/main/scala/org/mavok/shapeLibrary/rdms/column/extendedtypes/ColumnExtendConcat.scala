package org.mavok.shapeLibrary.rdms.column.extendedtypes

import org.mavok.api.column.{AbstractColumn, ColumnExtendConcat}
import org.mavok.api.table.DatasetReadable
import org.mavok.common.schematic.Schematic
import org.mavok.datatype.dbtype.DataType
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




class ColumnExtendConcatShape(val column: ColumnExtendConcat[_] ) extends ColumnGraphDefault {



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    val leftSide = new LeftColumnExtendConcatShape( column.leftBaseColumn )
    val rightSide = new RightColumnExtendConcatShape( column.baseColumn )
    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( leftSide )
      addShapeTo1stBuilder__Initialize( rightSide )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( leftSide, params )
      addShapeTo2ndBuilder__BuildFromContext( rightSide, params )
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, LEFT_PAREN )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[LeftColumnExtendConcatShape]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant see left side of column concat!"))
        .withKnown( Schematic.ONE, PIPE )
        .withKnown( Schematic.ONE, PIPE )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[RightColumnExtendConcatShape]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant see right side of column concat"))
        .withKnown( Schematic.ONE, SPACE )
        .withKnown( Schematic.ONE, RIGHT_PAREN )
        .withKnown( Schematic.ONE, SPACE )

  }


  private class ExtendConcatShape( val column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ColumnGraphDefault {



    override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

      val pointer: ColumnReferenceHeader = ColumnReferenceHeader( column, ColumnHeaderInitParams( addAlias = false, addComma = false , addSourceAlias =  true ) )
      protected override def initialize_1stBuilder(): Unit = {
        addShapeTo1stBuilder__Initialize( pointer )
      }

      protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
        addShapeTo2ndBuilder__BuildFromContext( pointer, params )
      }

      override def schematic(): Schematic =
        Schematic()
          .withKnown( Schematic.ONE, SPACE )
          .withFuture[ColumnReferenceHeader]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant find column for SQL header"))
          .withKnown( Schematic.ONE, SPACE )
    }

  }

  private class LeftColumnExtendConcatShape( column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ExtendConcatShape( column ) {}
  private class RightColumnExtendConcatShape( column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ExtendConcatShape( column ) {}

}




