package org.mavok.shapeLibrary.rdms.column.comparison

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.{ColumnGraph, ColumnGraphDefault}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{LEFT_PAREN, RIGHT_PAREN, SPACE}
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class ColumnWrapperComparisonGraph(  )
  extends ColumnGraphDefault {

  val column: ColumnGraph

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( column )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( column, params  )
    }


    import Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SPACE )
        .withKnown( ONE, LEFT_PAREN )
        .withFuture[ColumnGraph]( ONE, () =>  throw new IllegalArgumentException( "Not allowed to add column shape here as it needs binding") )
        .withKnown( ONE, RIGHT_PAREN )
        .withKnown( ONE, SPACE )
  }
}

object ColumnWrapperComparisonGraph {
  def lapply(_rColumn: ColumnGraph): LeftColumnWrapperComparisonGraph = new LeftColumnWrapperComparisonGraph() {
    override val column: ColumnGraph = _rColumn
  }

  def rapply(_rColumn: ColumnGraph): RightColumnWrapperComparisonGraph = new RightColumnWrapperComparisonGraph() {
    override val column: ColumnGraph = _rColumn
  }
}


abstract class LeftColumnWrapperComparisonGraph extends ColumnWrapperComparisonGraph{}
abstract class RightColumnWrapperComparisonGraph extends ColumnWrapperComparisonGraph{}



