package org.mavok.shapeLibrary.rdms.column.dataType.conversions

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.dataType.AbstractDDLType
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{LEFT_PAREN, RIGHT_PAREN, SPACE, SQLColumnExpression}

/*
 * Copyright: blacktest -- created 29.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object DDLVarchar {

  // ---------------------  Constructors

  def apply( _width: Int ): DDLVarchar = {
    new DDLVarchar( ){
      override def width: Int = _width
    }
  }

}

abstract class DDLVarchar extends AbstractDDLType {



  override val schematicOracle12: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "VARCHAR2") )
        .withKnown( ONE, LEFT_PAREN )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, SQLColumnExpression( width.toString )  )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, RIGHT_PAREN )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }


  override val schematicPostgres10: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "VARCHAR") )
        .withKnown( ONE, LEFT_PAREN )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, SQLColumnExpression( width.toString )  )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, RIGHT_PAREN )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    import org.mavok.common.schematic.Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SQLColumnExpression( "NVARCHAR") )
        .withKnown( ONE, LEFT_PAREN )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, SQLColumnExpression( width.toString )  )
        .withKnown( ONE, SPACE )
        .withKnown( ONE, RIGHT_PAREN )

    protected override def initialize_1stBuilder(): Unit = {

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}

  }



}

