package org.mavok.shapeLibrary.rdms.column.extendedtypes

import org.mavok.api.column.{AbstractColumn, ColumnExtendGetOrElse}
import org.mavok.api.table.DatasetReadable
import org.mavok.common.schematic.Schematic
import org.mavok.datatype.dbtype.DataType
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ColumnExtendCoalesceShape(val column: ColumnExtendGetOrElse[_ <: DataType] ) extends ColumnGraphDefault {



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    val leftSide = new LeftColumnExtendCoalesceShape( column.baseColumn )
    val rightSide = new RightColumnExtendCoalesceShape( column.rightBaseColumn )
    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( leftSide )
      addShapeTo1stBuilder__Initialize( rightSide )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( leftSide, params )
      addShapeTo2ndBuilder__BuildFromContext( rightSide, params )
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, COALESCE )
        .withKnown( Schematic.ONE, LEFT_PAREN )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[LeftColumnExtendCoalesceShape]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant see left side of column coalesce!"))
        .withKnown( Schematic.ONE, COMMA )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[RightColumnExtendCoalesceShape]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant see right side of column coalesce"))
        .withKnown( Schematic.ONE, SPACE )
        .withKnown( Schematic.ONE, RIGHT_PAREN )
        .withKnown( Schematic.ONE, SPACE )

  }


  private class ExtendConcatShape( val column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ColumnGraphDefault {



    override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

      val pointer: ColumnReferenceHeader = ColumnReferenceHeader( column, ColumnHeaderInitParams( addAlias = false, addComma = false , addSourceAlias =  true ) )
      protected override def initialize_1stBuilder(): Unit = {
        addShapeTo1stBuilder__Initialize( pointer )
      }

      protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
        addShapeTo2ndBuilder__BuildFromContext( pointer, params )
      }

      override def schematic(): Schematic =
        Schematic()
          .withKnown( Schematic.ONE, SPACE )
          .withFuture[ColumnReferenceHeader]( Schematic.ONE, () => throw new IllegalArgumentException( "Cant find column for SQL header"))
          .withKnown( Schematic.ONE, SPACE )
    }

  }

  private class LeftColumnExtendCoalesceShape( column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ExtendConcatShape( column ) {}
  private class RightColumnExtendCoalesceShape( column: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] ) extends ExtendConcatShape( column ) {}

}
