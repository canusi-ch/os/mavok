package org.mavok.shapeLibrary.rdms.column.transform

import org.mavok.api.column.AbstractColumn
import org.mavok.common.schematic.Schematic
import org.mavok.datatype.dbtype.DataType
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class MultiColumnGraph[T <: DataType,V](column: AbstractColumn[T, _] ) extends ColumnGraphDefault {
  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {
    protected override def initialize_1stBuilder(): Unit = ???

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = ???

    override def schematic(): Schematic = ???
  }
}








