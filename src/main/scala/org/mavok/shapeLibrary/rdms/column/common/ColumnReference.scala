package org.mavok.shapeLibrary.rdms.column.common

import org.mavok.api.column.{AbstractColumn, AbstractColumnExtender}
import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.ColumnGraphDefault
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, Mapping, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.SQLColumnExpression

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




object ColumnReference {
  def apply(  column: AbstractColumn[_, _]): ColumnReference = { new ColumnReference(  column ) }
}


class ColumnReference(val column: AbstractColumn[_, _] ) extends ColumnGraphDefault {

  val self = this

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {

      if( params.hasOwnedResource( column ) ){
        column match {
          case column: AbstractColumnExtender[_] => throw new IllegalArgumentException( "Not allowed to be a base column extender")
          case _ => addShapeTo2ndBuilder__BuildFromContext( SQLColumnExpression( column.columnPointer ), params )
        }
      } else {
        addShapeTo2ndBuilder__BuildFromContext( ColumnAlias( params.getAlias( column ) ), params )
      }

    }

    override def schematic(): Schematic =
      Schematic()
        .withFuture[Mapping]( Schematic.ONE, () => throw new IllegalArgumentException( "Not allowed to set alias at compile time"))

  }

}
