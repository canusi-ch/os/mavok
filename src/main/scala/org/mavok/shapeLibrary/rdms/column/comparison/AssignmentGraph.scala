package org.mavok.shapeLibrary.rdms.column.comparison

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.rdms.column.{ColumnGraph, ColumnGraphDefault}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{COMMA, NEWLINE, _equals}
/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class AssignmentGraph(lColumn: ColumnGraph, rColumn: ColumnGraph , addComma: Boolean  )extends ColumnGraphDefault {
  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {


    val leftColumnMem = ColumnWrapperComparisonGraph.lapply( lColumn )
    val rightColumnMem = ColumnWrapperComparisonGraph.rapply( rColumn )


    protected override def initialize_1stBuilder(): Unit = {

      addShapeTo1stBuilder__Initialize( leftColumnMem )
      addShapeTo1stBuilder__Initialize( rightColumnMem )

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( leftColumnMem, params )
      addShapeTo2ndBuilder__BuildFromContext( rightColumnMem, params )
      if( addComma ) addShapeTo2ndBuilder__BuildFromContext( COMMA )
    }




    import Schematic._
    override def schematic(): Schematic =
      Schematic()
        .withFuture[COMMA.type]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "Wrong count of commas found") )
        .withFuture[LeftColumnWrapperComparisonGraph]( ONE, () =>  throw new IllegalArgumentException( "Not allowed to add column shape here as it needs binding") )
        .withKnown( ONE, NEWLINE )
        .withKnown( ONE, _equals )
        .withFuture[RightColumnWrapperComparisonGraph]( ONE, () =>  throw new IllegalArgumentException( "Not allowed to add column shape here as it needs binding") )


  }
}


object AssignmentGraph {

  def apply(lColumn: ColumnGraph, rColumn: ColumnGraph, addComma: Boolean = false ): AssignmentGraph = new AssignmentGraph(lColumn, rColumn, addComma )
}







