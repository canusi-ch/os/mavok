package org.mavok.shapeLibrary.rdms.column

import org.mavok.resource.HasGraphResourceManager
import org.mavok.shapeLibrary.rdms.column.common.ColumnHeaderInitParams
import org.mavok.shapeLibrary.rdms.common.{Graph, GraphDBSchematicShape, ScopedGraph}

/*
 * Copyright: blacktest -- created 29.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait ColumnGraph extends Graph with HasGraphResourceManager {

}
