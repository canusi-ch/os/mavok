package org.mavok.shapeLibrary.rdms.column.common

/*
 * Copyright: blacktest -- created 30.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




// todo implement this in column extended types
trait ReferenceParams {

  val setTableAlias: Boolean
  val setColumnAlias: Boolean
  val setComma: Boolean

}
