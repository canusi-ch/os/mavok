package org.mavok.shapeLibrary.rdms.keywords

import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.SQL
import org.mavok.shapeLibrary.rdms.common.{MapTrail, Mapping}

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



class KeyWord(sql: String  )
  extends Mapping{
  final override def map( input: ConnectionType.Value, mapTrail: MapTrail = MapTrail( ListBuffer[String](), 0 ) ): Seq[SQL] = Seq( SQL( sql ) )

}


object COALESCE extends KeyWord( "COALESCE" )

object ALTER extends KeyWord( "ALTER" )
object COLUMN extends KeyWord( "COLUMN" )
object ADD extends KeyWord( "ADD" )
object PRIMARYKEY extends KeyWord( "PRIMARY KEY" )




object CREATE extends KeyWord( "CREATE" )
object COPY extends KeyWord( "COPY" )
object TABLE extends KeyWord( "TABLE" )

object SELECT extends KeyWord( "SELECT" )
object FROM extends KeyWord( "FROM" )


object WHERE extends KeyWord( "WHERE" )
object EXISTS extends KeyWord( "EXISTS" )



object ORDER_BY extends KeyWord( "ORDER BY" )
object GROUP_BY extends KeyWord( "GROUP BY" )

object LEFT_PAREN extends KeyWord( "(" )
object RIGHT_PAREN extends KeyWord( ")" )
object PERIOD extends KeyWord( "." )
object SPACE extends KeyWord( " " )
object NEWLINE extends KeyWord( "\n" )
object COMMA extends KeyWord( "," )


object DROP extends KeyWord( "DROP" )
object TRUNCATE extends KeyWord( "TRUNCATE" )

object INSERT extends KeyWord( "INSERT" )
object VALUES extends KeyWord( "VALUES" )

object UPDATE extends KeyWord( "UPDATE" )
object SET extends KeyWord( "SET" )

object MAX extends KeyWord( "MAX" )
object MIN extends KeyWord( "MIN" )
object COUNT extends KeyWord( "COUNT" )
object CAST extends KeyWord( "CAST" )
object AS extends KeyWord( "AS" )
object PIPE extends KeyWord( "|" )

object JOIN extends KeyWord( "JOIN" )



object MERGE extends KeyWord( "MERGE" )
object INTO extends KeyWord( "INTO" )
object USING extends KeyWord( "USING" )
object WHEN extends KeyWord( "WHEN" )
object THEN extends KeyWord( "THEN" )
object NOT extends KeyWord( "NOT" )
object MATCHED extends KeyWord( "MATCHED" )