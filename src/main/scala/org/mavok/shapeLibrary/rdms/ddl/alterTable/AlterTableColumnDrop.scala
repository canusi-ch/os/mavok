package org.mavok.shapeLibrary.rdms.ddl.alterTable

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetPersistent
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._


/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class AlterTableColumnDrop extends ScopedGraph with DBSchematicContractCommon {

  val table: DatasetPersistent
  val column: AbstractColumn[ _, _ ]

  private val self = this

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    override def schematic(): Schematic =
      Schematic()
      .withKnown( ONE, {ALTER})
      .withKnown( ONE, {SPACE})
      .withKnown( ONE, {TABLE})
      .withKnown( ONE, {SPACE})
      .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No DBObject found"))
      .withKnown( ONE, SPACE )
      .withKnown( ONE, DROP )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, COLUMN)
      .withKnown( ONE, SPACE )
      .withKnown( ONE, SQLColumnExpression( column.columnPointer )  )


    protected override def initialize_1stBuilder(): Unit = {
      val tableShape = table.asShape

      val tableRef = DBObject( tableShape, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      addShapeTo2ndBuilder__BuildFromContext( tableRef )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}
  }
}






