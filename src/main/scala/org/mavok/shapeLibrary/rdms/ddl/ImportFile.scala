package org.mavok.shapeLibrary.rdms.ddl

import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.DatasetContainer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class ImportFile extends ScopedGraph with DBSchematicContractCommon {

  val filePath: String
  val delimiter: String
  val table: DatasetContainer[_]

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    override def schematic(): Schematic =
      Schematic()
      .withKnown( ONE, {COPY})
      .withKnown( ONE, {SPACE})
      .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No DBObject found"))
      .withKnown( ONE, {SPACE})
      .withKnown( ONE, FROM )
      .withKnown( ONE, {SPACE})
      .withKnown( ONE, SQLColumnExpression( "'" + filePath + "'"))
      .withKnown( ONE, {SPACE})
      .withKnown( ONE, SQLColumnExpression( " DELIMITERS '" + delimiter + "' CSV HEADER" ))


    protected override def initialize_1stBuilder(): Unit = {
      val tableRef = DBObject( table, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      addShapeTo2ndBuilder__BuildFromContext( tableRef )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}
  }
}

object ImportFile {

  def apply( _filePath: String, _delimiter: String, _table: DatasetContainer[_] ): ImportFile = new ImportFile {
    override val filePath: String = _filePath
    override val delimiter: String = _delimiter
    override val table: DatasetContainer[_] = _table
    override val graphScope: Scope = Scope()
  }

}




