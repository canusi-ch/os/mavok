package org.mavok.shapeLibrary.rdms.ddl

import org.mavok.api.column.Column
import org.mavok.api.table.DatasetReadable
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.datatype.dbtype.DataType
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, Mapping, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class SimpleColumnNameWithDDL extends ScopedGraph with DBSchematicContractCommon {

  val column: Column[_ <: DataType, _ <: DatasetReadable[_]]
  val addComma: Boolean

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    private var table: ColumnContainerGraphShape = _

    override def schematic(): Schematic =
      Schematic()
      .withKnown[Mapping]( ONE, if( addComma ) COMMA else SPACE   )
      .withKnown( ONE, SQLColumnExpression( column.name.name ) ) // todo this needs better solution
      .withKnown( ONE, SPACE )
      .withKnown( ONE, column.dbType.toDDL )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}
  }
}

object SimpleColumnNameWithDDL {
  def apply(_column: Column[_ <: DataType, _ <: DatasetReadable[_]], _addComma: Boolean ): SimpleColumnNameWithDDL = new SimpleColumnNameWithDDL() {
    override val column: Column[_ <: DataType, _ <: DatasetReadable[_]] = _column
    override val addComma: Boolean = _addComma
    override val graphScope: Scope = Scope()
  }
}




