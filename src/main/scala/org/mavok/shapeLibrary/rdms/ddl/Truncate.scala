package org.mavok.shapeLibrary.rdms.ddl

import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic.ONE
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.DatasetContainer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class Truncate extends ScopedGraph with DBSchematicContractCommon {

  protected val table: DatasetContainer[_]

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {


    override def schematic(): Schematic =
      Schematic()
        .withKnown[TRUNCATE.type]( ONE,  {TRUNCATE})
        .withKnown[SPACE.type]( ONE,  {SPACE})
        .withKnown[TABLE.type]( ONE,  {TABLE})
        .withKnown[SPACE.type]( ONE,  {SPACE})
        .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No dboject found"))

    protected override def initialize_1stBuilder(): Unit = {
      val tableRef = DBObject( table, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      addShapeTo2ndBuilder__BuildFromContext( tableRef )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager ): Unit = {}
  }


}

object Truncate {
  def apply(_table:DatasetContainer[_], scope: Scope = Scope() ): Truncate = new Truncate{
    override val graphScope: Scope = scope
    override protected val table: DatasetContainer[_] = _table
  }
}




