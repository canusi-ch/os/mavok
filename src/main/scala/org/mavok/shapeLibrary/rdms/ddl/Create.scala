package org.mavok.shapeLibrary.rdms.ddl

import org.mavok.api.table.AbstractTable
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.DatasetContainer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




object Create {

  def apply(_table: DatasetContainer[_], scope: Scope = Scope() ): Create = new Create{
    override val graphScope: Scope = scope
    override val table: DatasetContainer[_] = _table
  }
  
}




abstract class Create extends ScopedGraph with DBSchematicContractCommon {

  val table: DatasetContainer[_]

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    override def schematic(): Schematic =
      Schematic()
        .withKnown(ONE, CREATE)
        .withKnown(ONE, SPACE)
        .withKnown(ONE, TABLE)
        .withKnown(ONE, SPACE)
        .withFuture[DBObject](ONE, () => throw new IllegalArgumentException("No DBObject found"))
        .withKnown(ONE, LEFT_PAREN)
        .withFuture[SimpleColumnNameWithDDL](ONE_OR_MORE, () => throw new IllegalArgumentException("No Columns found"))
        .withKnown(ONE, RIGHT_PAREN)

    protected override def initialize_1stBuilder(): Unit = {
      val tableRef = DBObject(table, graphScope)
      addShapeTo1stBuilder__Initialize(tableRef)
      addShapeTo2ndBuilder__BuildFromContext(tableRef)

      table.sourceObject match {
        case dbTable: AbstractTable => {
          for (i <- dbTable.getColumns.indices) {
            val column = dbTable.getColumns(i)
            val ctColumn = SimpleColumnNameWithDDL(column, if (i == 0) false else true)
            addShapeTo1stBuilder__Initialize(ctColumn)
            addShapeTo2ndBuilder__BuildFromContext(ctColumn)
          }
        }
        case _ => throw new IllegalArgumentException(s"Not allowed to create a virtual table ${table.sourceObject}")
      }
    }


    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {}

  }
}




