package org.mavok.shapeLibrary.rdms.dbobject

import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object DBObject {

  def apply( source: ColumnContainerGraphShape, scope: Scope  ): DBObject = new DBObject{
    override val graphScope: Scope = scope
    override protected val sourceShape: ColumnContainerGraphShape = source
  }

}



abstract class DBObject extends Sourceable {

  override val graphScope: Scope
  protected val sourceShape: ColumnContainerGraphShape

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {
    override def schematic(): Schematic =
      Schematic()
        .withFuture[ColumnContainerGraphShape]( Schematic.ONE, () => throw new IllegalArgumentException("No table reference found") )

    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( sourceShape )
      addShapeTo2ndBuilder__BuildFromContext( sourceShape )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {

    }
  }




}

