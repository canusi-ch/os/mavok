package org.mavok.shapeLibrary.rdms.segment

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.comparison.ColumnComparison
import org.mavok.shapeLibrary.rdms.common.{Graph, GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.keywords.{NEWLINE, SPACE, WHERE}

/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object Where {

  // ---------------------  Constructors

  def apply(_comparisonGraph: ColumnComparison): Where = {
    new Where() {
      override protected val comparisonGraph: ColumnComparison = _comparisonGraph
    }
  }

}


abstract class Where extends Graph with DBSchematicContractCommon {

  protected val comparisonGraph: ColumnComparison

  override val defaultSchematic = new GraphDBSchematicShape{

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, {SPACE} )
        .withKnown( Schematic.ONE, {NEWLINE} )
        .withKnown[WHERE.type]( Schematic.ONE, {WHERE} )
        .withFuture[ColumnComparison]( Schematic.ONE, () =>
        throw new IllegalArgumentException( "No Comparison clause found in where statement") )

    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( comparisonGraph )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( comparisonGraph, params )
    }
  }



}