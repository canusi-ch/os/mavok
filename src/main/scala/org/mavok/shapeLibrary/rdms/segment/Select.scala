package org.mavok.shapeLibrary.rdms.segment

import org.mavok.api.column.AbstractColumn
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.keywords.{SELECT, SPACE}

import scala.collection.mutable.ListBuffer


/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


case class SelectParams(query: ScopedGraph, columns: Seq[AbstractColumn[_,_]] )

object Select {

  // ---------------------  Constructors

  def apply( scope: Scope, params: SelectParams ): Select = {
    new Select() {
      override val graphScope: Scope = scope
      protected val selectParams: SelectParams = params
    }
  }

}

abstract class Select extends ScopedGraph with DBSchematicContractCommon {


  protected val selectParams: SelectParams

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    private var columnHeaders: Seq[ColumnReferenceHeader]  = _

    override def schematic(): Schematic =
      Schematic()
        .withKnown[SELECT.type]( Schematic.ONE, SELECT )
        .withKnown[SPACE.type]( Schematic.ONE, SPACE )
        .withFuture[ColumnReferenceHeader]( Schematic.ONE_OR_MORE, () => throw new IllegalArgumentException( s"No columns found. " +
        s"ColumnArray set to ${columnHeaders.mkString}" ) )
        .withKnown[SPACE.type]( Schematic.ONE, SPACE )

    protected override def initialize_1stBuilder(): Unit = {
      require( this.columnHeaders == null, "Not allowed to call preInitialize a 2nd time" )
      require( selectParams.columns.nonEmpty, "Select statement must have at least 1 column!")


      val listColumnHeaders: ListBuffer[ColumnReferenceHeader] = ListBuffer[ColumnReferenceHeader]()
      for( i <- selectParams.columns.indices ){
        listColumnHeaders += ColumnReferenceHeader( selectParams.columns(i)
          , ColumnHeaderInitParams( addAlias = true, addComma = {if( i == 0 ) false else true } ))
      }

      this.columnHeaders = listColumnHeaders
      for( i <- columnHeaders.indices ){
        val columnHeader = columnHeaders(i)
        require( columnHeader != null )
        addShapeTo1stBuilder__Initialize( columnHeader )
      }
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      columnHeaders.foreach{
        columnHeader =>
        addShapeTo2ndBuilder__BuildFromContext( columnHeader, params )
      }

    }

  }
}

