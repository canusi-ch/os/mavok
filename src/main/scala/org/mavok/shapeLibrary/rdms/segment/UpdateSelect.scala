package org.mavok.shapeLibrary.rdms.segment

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.column.comparison.AssignmentGraph
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dml.Query
import org.mavok.shapeLibrary.rdms.keywords._

import scala.collection.mutable.ListBuffer


/*
 * Copyright: blacktest -- created 24.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



object UpdateSelect {

  // ---------------------  Constructors

  def apply(scope: Scope, _columnMappings: Seq[( Column[_, _], AbstractColumn[_, _]) ], _sourceQuery: Query ): UpdateSelect = {
    new UpdateSelect() {
      override val graphScope: Scope = scope
      protected val columnMappings: Seq[( Column[_, _], AbstractColumn[_, _]) ] = _columnMappings
      override protected val sourceQuery: Query = _sourceQuery
    }
  }

}

abstract class UpdateSelect extends ScopedGraph with DBSchematicContractCommon {


  private val self = this
  override val graphScope: Scope
  protected val columnMappings: Seq[( Column[_, _], AbstractColumn[_, _]) ]
  protected val sourceQuery: Query

  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape{

    private val listAssignmentGraphs: ListBuffer[AssignmentGraph] = ListBuffer[AssignmentGraph]()

    override def schematic(): Schematic =
      Schematic()
        .withFuture[AssignmentGraph]( Schematic.ONE_OR_MORE, () => throw new IllegalArgumentException( "Never found the assignents..."))
        .withKnown( Schematic.ONE, SPACE )
        .withKnown( Schematic.ONE, NEWLINE )
        .withKnown( Schematic.ONE, FROM )
        .withKnown( Schematic.ONE, SPACE )
        .withKnown( Schematic.ONE, NEWLINE )
        .withKnown( Schematic.ONE, LEFT_PAREN )
        .withFuture[Query]( Schematic.ONE_OR_MORE, () => throw new IllegalArgumentException( s"No query found" ) )
        .withKnown( Schematic.ONE, RIGHT_PAREN )
        .withKnown( Schematic.ONE, SPACE )
        .withFuture[Alias]( Schematic.ONE, () => throw new IllegalArgumentException( "Couldnt find alias for query"))

    protected override def initialize_1stBuilder(): Unit = {
      addShapeTo1stBuilder__Initialize( sourceQuery )

      for( i<- columnMappings.indices ){
        val mapping = columnMappings(i)
        val target = ColumnReferenceHeader( mapping._1, ColumnHeaderInitParams( addAlias = false, addComma = false, addSourceAlias = false ))
        val source = ColumnReferenceHeader( mapping._2, ColumnHeaderInitParams( addAlias = false, addComma = false, addSourceAlias = true ))
        listAssignmentGraphs += AssignmentGraph( target, source, if( i == 0 ) false else true )



      }

      listAssignmentGraphs.foreach{
        assignmentGraph => addShapeTo1stBuilder__Initialize( assignmentGraph )
      }



    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {

      addShapeTo2ndBuilder__BuildFromContext( sourceQuery, null )

      params.addChildAliasManager( sourceQuery )
      addShapeTo2ndBuilder__BuildFromContext( params.getAlias( sourceQuery ) )


      listAssignmentGraphs.foreach{
        assignmentGraph => addShapeTo2ndBuilder__BuildFromContext( assignmentGraph , params  )
      }


    }

  }
}

