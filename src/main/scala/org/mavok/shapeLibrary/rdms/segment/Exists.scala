package org.mavok.shapeLibrary.rdms.segment

import org.mavok.common.schematic.Schematic
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.common.{Graph, GraphDBSchematicShape, ScopeResourceManager}
import org.mavok.shapeLibrary.rdms.dml.Query
import org.mavok.shapeLibrary.rdms.keywords._

/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class Exists extends Graph with DBSchematicContractCommon {

  protected val query: Query

  override val defaultSchematic = new GraphDBSchematicShape{

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, {SPACE} )
        .withKnown( Schematic.ONE, {NEWLINE} )
        .withKnown( Schematic.ONE, {EXISTS} )
        .withKnown( Schematic.ONE, {SPACE} )
        .withKnown( Schematic.ONE, {LEFT_PAREN} )
        .withFuture[Query]( Schematic.ONE, () =>
        throw new IllegalArgumentException( "No Comparison clause found in where statement") )
        .withKnown( Schematic.ONE, {RIGHT_PAREN} )

    protected override def initialize_1stBuilder(): Unit = {}

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( query )
    }
  }

}

object Exists {

  def apply( _query: Query ): Exists = new Exists(){
    override protected val query: Query = _query
  }
}