package org.mavok.shapeLibrary.rdms.dml.rdbms

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContract
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.rdms.dml.Query
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.rdms.segment.{Exists, UpdateSelect, Where}
import org.mavok.workflowEngine.{ColumnCompositeComparisonFlow, QueryFlow}

import scala.collection.mutable.ListBuffer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */





abstract class SetColumnListToQuery extends ScopedGraph with DBSchematicContract {

  protected val sourceQuery: QueryFlow[_]
  protected val columnMap: Seq[( Column[_, _], AbstractColumn[_, _])]
  protected val onCondition: ColumnCompositeComparisonFlow


  private val self = this

  override lazy val schematicOracle12: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected val source = {
      sourceQuery.where(  onCondition )
      sourceQuery.assembleGraph( graphScope )
    }
    protected val columnHeaders: ListBuffer[ColumnReferenceHeader] = ListBuffer[ColumnReferenceHeader]()

    override def schematic(): Schematic =
      Schematic()
      .withKnown( ONE, SET )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, LEFT_PAREN )
      .withFuture[ColumnReferenceHeader]( ONE_OR_MORE, () => throw new IllegalArgumentException( "Columns need to be defined for update set segment!")  )
      .withKnown( ONE, RIGHT_PAREN )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, _equals )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, LEFT_PAREN )
      .withFuture[Query]( ONE, () => throw new IllegalArgumentException( "Update assignment query not found!"))
      .withKnown( ONE, RIGHT_PAREN )
      .withKnown( ONE, NEWLINE )
      .withKnown( ONE, WHERE )
      .withFuture[Exists]( ONE, () => throw new IllegalArgumentException( "No exists statement found"))



    protected override def initialize_1stBuilder(): Unit = {

      areInputsValid()
      val columnTarget = columnMap.map( _._1 )
      for( i<- columnTarget.indices ){
        val columnHeader = ColumnReferenceHeader( columnTarget(i), ColumnHeaderInitParams( addAlias = false, addComma = {if( i == 0 ) false else true }, addSourceAlias = false ))
        addShapeTo1stBuilder__Initialize( columnHeader )
        columnHeaders += columnHeader
      }

      addShapeTo1stBuilder__Initialize( source )

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      columnHeaders.foreach{
        addShapeTo2ndBuilder__BuildFromContext( _, params )
      }

      addShapeTo2ndBuilder__BuildFromContext( source, null  )
      addShapeTo2ndBuilder__BuildFromContext( Exists( source ) )
    }
  }



  override lazy val schematicPostgres10: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected val source = {
      sourceQuery.assembleGraph( graphScope )
    }

    private val where = Where( onCondition.expressionColumnGraph )

    protected val updateSelect: UpdateSelect = UpdateSelect(  graphScope, columnMap, source )

    override def schematic(): Schematic =
      Schematic()
        .withKnown( ONE, SET )
        .withKnown( ONE, SPACE )
        .withFuture[UpdateSelect]( ONE_OR_MORE, () => throw new IllegalArgumentException( "Columns need to be defined for update set segment!") )
        .withKnown( ONE, NEWLINE)
        .withFuture[Where]( ZERO_OR_ONE, () => throw new IllegalArgumentException( "No where statement found") )



    protected override def initialize_1stBuilder(): Unit = {

      areInputsValid()
      addShapeTo1stBuilder__Initialize( updateSelect )
      addShapeTo1stBuilder__Initialize( where )
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( updateSelect, params )
      addShapeTo2ndBuilder__BuildFromContext( where, params  )
    }
  }




  override lazy val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {


    private val columnHeaders: ListBuffer[ColumnReferenceHeader] = ListBuffer[ColumnReferenceHeader]()

    override def schematic(): Schematic = ???
    protected override def initialize_1stBuilder(): Unit = ???

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = ???
  }



  private def areInputsValid(): Boolean = {
/* todo
    columnMap.foreach( columnMapping => {
      require( source.columns.contains( columnMapping._2 ), s"${columnMapping._2} not found in source. " +
        s"Source columns: ${source.columns}"+
        s"Target column mappings: ${columnMap}" )
    }
    )*/
    true
  }

}

object SetColumnListToQuery {

  def apply( _source: QueryFlow[_]
             , _columnMap: Seq[(Column[_, _], AbstractColumn[_, _])]
             , _graphSCope: Scope
             , _onCondition: ColumnCompositeComparisonFlow
           ): SetColumnListToQuery = new SetColumnListToQuery() {
    override val sourceQuery: QueryFlow[_] = _source
    override val columnMap: Seq[(Column[_, _], AbstractColumn[_, _])] = _columnMap
    override val graphScope: Scope = _graphSCope
    override protected val onCondition: ColumnCompositeComparisonFlow = _onCondition
  }


}






