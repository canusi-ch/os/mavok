package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.Column
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.{ColumnHeaderInitParams, ColumnReferenceHeader}
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape

import scala.collection.mutable.ListBuffer



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


abstract class Insert extends ScopedGraph with DBSchematicContractCommon  {

  private val self = this


  val insertQuery: ColumnContainerGraphShape
  val columns: Seq[Column[_, _]]

  var tableRef: DBObject = _

  override lazy val defaultSchematic = new GraphDBSchematicShape {

    val listColumnPointers: ListBuffer[ColumnReferenceHeader]  =  ListBuffer[ColumnReferenceHeader]()

    override def schematic(): Schematic =
    Schematic()
      .withKnown( ONE, INSERT )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, INTO )
      .withKnown( ONE, SPACE )
      .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No table found!") )
      .withKnown( ONE, LEFT_PAREN )
      .withKnown( ONE, SPACE )
      .withFuture[ColumnReferenceHeader]( ONE_OR_MORE, () => throw new IllegalArgumentException( "No columns for insert found") )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, RIGHT_PAREN)
      .withKnown( ONE, SPACE )
      .withFuture[ColumnContainerGraphShape]( ONE, () => throw new IllegalArgumentException( "No Query in builder setup") )

    protected override def initialize_1stBuilder(): Unit = {

      addShapeTo1stBuilder__Initialize( insertQuery )

      var insertTable: ColumnContainerGraphShape = null




      for( i<- columns.indices ) {
        val column = columns(i)
        // todo remove put in subquery
        if (insertTable == null) {
          insertTable = column.source.asShape
        }
        else if (insertTable != column.source.asShape) {
          throw new IllegalArgumentException(
            s"Not allowed to have multiple tables in insert statement. Found ${columns.map(_.source.asShape).mkString("|")}")
        }
      }

      tableRef = DBObject( insertTable, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      self.addChildAliasManager( tableRef )

      for( i<- columns.indices ) {
        val column = columns(i)
        tableRef.graphResourceManager.addResource( column )
        tableRef.addOwnedResource( column )
        val pointer = ColumnReferenceHeader(column, ColumnHeaderInitParams( addAlias = false, addComma = { if( i == 0 ) false else true }, addSourceAlias = false))

        addShapeTo1stBuilder__Initialize(pointer)
        listColumnPointers += pointer

      }




    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
      addShapeTo2ndBuilder__BuildFromContext( insertQuery )
      require( listColumnPointers.nonEmpty )
      listColumnPointers.foreach{
        column =>
        addShapeTo2ndBuilder__BuildFromContext( column , self)
      }

      addShapeTo2ndBuilder__BuildFromContext( tableRef, self  )
    }

  }


}

object Insert {
  def apply( _scope: Scope, _insertQuery: ColumnContainerGraphShape, _columns: Seq[Column[_, _]] ): Insert = new Insert(){
    override val graphScope: Scope = _scope
    override val insertQuery: ColumnContainerGraphShape = _insertQuery
    override val columns: Seq[Column[_, _]] = _columns
  }
}




