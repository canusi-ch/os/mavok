package org.mavok.shapeLibrary.rdms.dml

import org.mavok.api.column.Column
import org.mavok.common.schematic.Schematic
import org.mavok.common.schematic.Schematic._
import org.mavok.shapeLibrary.dbcontract.DBSchematicContractCommon
import org.mavok.shapeLibrary.rdms.column.common.ColumnReference
import org.mavok.shapeLibrary.rdms.column.comparison.ComparisonGraph
import org.mavok.shapeLibrary.rdms.common._
import org.mavok.shapeLibrary.rdms.dbobject.DBObject
import org.mavok.shapeLibrary.rdms.keywords._
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




abstract class Merge extends ScopedGraph with DBSchematicContractCommon {

  private val self = this


  val insertQuery: ColumnContainerGraphShape
  val insertColumns: Seq[Column[_, _]]

  override lazy val defaultSchematic = new GraphDBSchematicShape {

    override def schematic(): Schematic =
    Schematic()
      .withKnown( ONE, MERGE )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, INTO )
      .withFuture[DBObject]( ONE, () => throw new IllegalArgumentException( "No table found!") )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, USING )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, LEFT_PAREN)
      .withFuture[Sourceable]( ONE, () => throw new IllegalArgumentException( "No source found") )
      .withKnown( ONE, RIGHT_PAREN )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, ON )
      .withKnown( ONE, SPACE )
      .withFuture[ComparisonGraph]( ONE, () => throw new IllegalArgumentException( "No match found") )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, WHEN )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, MATCHED )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, THEN )
      .withKnown( ONE, SPACE )
      .withFuture[Update]( ONE, () => throw new IllegalArgumentException( "No update segment found") )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, WHEN )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, NOT )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, MATCHED )
      .withKnown( ONE, SPACE )
      .withKnown( ONE, THEN )
      .withKnown( ONE, SPACE )
      .withFuture[Insert]( ONE, () => throw new IllegalArgumentException( "No insert segment found") )

    protected override def initialize_1stBuilder(): Unit = {

      var insertTable: ColumnContainerGraphShape = null

      insertColumns.foreach(
        mapping => {

          // todo remove put in subquery
          if( insertTable == null ) {
            insertTable = mapping.source.asShape
          }
          else if( insertTable != mapping.source.asShape ){
            throw new IllegalArgumentException(
              s"Not allowed to have multiple tables in insert statement. Found ${insertColumns.map( _.source.asShape).mkString("|")}")
          }
        }
      )


      val tableRef = DBObject( insertTable, graphScope )
      addShapeTo1stBuilder__Initialize( tableRef )
      addShapeTo2ndBuilder__BuildFromContext( insertQuery )

      for( i <- insertColumns.indices ){
        val pointer = ColumnReference( insertColumns( i ) )
        addShapeTo1stBuilder__Initialize( pointer )
        addShapeTo2ndBuilder__BuildFromContext( pointer, self )
      }

    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {

    }


  }




}






