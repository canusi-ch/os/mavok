package org.mavok.shapeLibrary.rdms.common

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.SQL





trait Mapping  {

  def map( input: ConnectionType.Value, mapTrail: MapTrail ): Seq[SQL]


}




