package org.mavok.shapeLibrary.rdms.common

import org.mavok.common.schematic.Schematic
import org.mavok.common.tools.Memoize

/*
 * Copyright: blacktest -- created 12.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait DBSchematicShape[BindParams, BuildInput, BuildOutput] {



  private var bindParams: BindParams = _

  /** init and bind functions only allowed to be called once
   */
  private lazy val initMemoize = Memoize( (dummy: Int) => initialize_1stBuilder() )
  private lazy val bindMemoize = Memoize( (params: BindParams ) => bindInContext_2ndBuilder(params) )



  def schematic(): Schematic


  def build(params: BuildInput ): BuildOutput


  protected def initialize_1stBuilder(): Unit

  protected def bindInContext_2ndBuilder(params: BindParams ): Unit



  private[common] final def setBindingParams( params: BindParams ): Unit = synchronized {
    bindParams = params
  }

  private[common] def setupInitBuild(): Unit = {
    initMemoize.apply( 0 )
  }


  private[common] def setupBind(): Unit = {
    bindMemoize.apply(bindParams)
  }

}




