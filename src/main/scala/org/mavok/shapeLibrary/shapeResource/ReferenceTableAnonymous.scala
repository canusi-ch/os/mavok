package org.mavok.shapeLibrary.shapeResource

import org.mavok.common.schematic.Schematic
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.dbcontract.DBSchematicContract
import org.mavok.shapeLibrary.rdms.common.{GraphDBSchematicShape, ScopeResourceManager, ScopedGraph}
import org.mavok.shapeLibrary.rdms.keywords.SQLTablePointer

/*
 * Copyright: blacktest -- created 05.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait ReferenceTableAnonymous extends ScopedGraph with DBSchematicContract {



  override val defaultSchematic: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
    }

    override def schematic(): Schematic =
      Schematic()
        .withFuture[SQLTablePointer](Schematic.ONE, () => throw new IllegalStateException("No pointer was setup for this table")
      )

  }


  override val schematicOracle12: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, SQLTablePointer( "DUAL"))

  }


  override val schematicPostgres10: GraphDBSchematicShape = new GraphDBSchematicShape {

    protected override def initialize_1stBuilder(): Unit = {
    }

    protected override def bindInContext_2ndBuilder(params: ScopeResourceManager): Unit = {
    }

    override def schematic(): Schematic =
      Schematic()
        .withKnown( Schematic.ONE, SQLTablePointer( "( SELECT 1 )"))


  }
}

object ReferenceTableAnonymous {

  def apply( scope: Scope = Scope() ): ReferenceTableAnonymous = new ReferenceTableAnonymous(){
    override val graphScope: Scope = scope
  }
}


