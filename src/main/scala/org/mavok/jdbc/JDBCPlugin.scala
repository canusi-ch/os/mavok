package org.mavok.jdbc

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import akka.stream.scaladsl.{Sink, Source}
import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.mavok.LOG
import org.mavok.api.table.TableModelGeneric
import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.CaseClassFactory
import org.mavok.common.classvalue.CaseClassFactory.SupportedCaseTypes
import org.mavok.datatype.dbtype._
import slick.jdbc.{GetResult, PositionedResult}

import scala.collection.immutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success, Try}

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
import scala.reflect.runtime.universe._




trait JDBCPlugin {


  def getConnectionType: ConnectionType.Value = connectionType

  val connectionType: ConnectionType.Value
  val configName: String

  lazy implicit val system = ActorSystem( configName + "_actor" )
  lazy implicit val materializer = ActorMaterializer()
  lazy implicit val session = {
    val newSession = SlickSession.forConfig(configName)
    system.registerOnTermination(newSession.close())
    newSession
  }




  import session.profile.api._

  def execQuery[ExpectedOutput <: Product](   sqlStatement: String
                                              , handler: ExpectedOutput => Unit
                                          )( implicit typeTag: TypeTag[ExpectedOutput] ): Unit = {

    val classFactory: CaseClassFactory[ExpectedOutput] = CaseClassFactory[ExpectedOutput]()

    implicit val transformResult: GetResult[ExpectedOutput] = GetResult( {( r: PositionedResult )  => {
      val result = new Array[Any](r.numColumns)
      for( i <- 0 until r.numColumns ){
        result(i) = getCastedValueAt(r, classFactory )
      }
      val next = classFactory.newInstance( result.map( _.asInstanceOf[AnyRef]) )
      next
    }}
    )

    val done: Future[Done] =
      Slick
        .source( sql"#$sqlStatement".as[ExpectedOutput] )
        .log( s"${classFactory.getClass}")
        .runWith( Sink.foreach( handler( _ )) )


    Await.result(
      done, Duration.Inf
    )  match {
      case onSuccess => LOG.debug( "Finished ", sqlStatement )
      case onFailure => throw new IllegalStateException(s"Failed to finish: ${sqlStatement}")
    }

  }

  def queryGeneric(   sqlStatement: String
                    , table: TableModelGeneric
                    , handler: GenericRecord => Unit
            ): Unit = {

    val partiallyCompiledGenericRecord = new GenericData.Record( _: Schema )

    implicit val transformResult: GetResult[GenericRecord] = GetResult( ( r: PositionedResult )  => {
        val genericRecord: GenericRecord = partiallyCompiledGenericRecord( table.avroSchema.schema )
        for( i <- 0 until r.numColumns ){
          genericRecord.put(
            table.avroSchema.fieldNames( i ).name
            , mapResultSetToDBType( table.avroSchema.fieldTypes(i), r  ) )
        }
        genericRecord
      }
    )

    val done: Future[Done] =
      Slick
        .source( sql"#$sqlStatement".as[GenericRecord] )
        .log( s"${table.tablePath.getTablePath}")
        .runWith( Sink.foreach( handler( _ )) )


    Await.result(
      done, Duration.Inf
    )  match {
      case onSuccess => LOG.debug( "Finished ", sqlStatement )
      case onFailure => throw new IllegalStateException(s"Failed to finish: ${sqlStatement}")
    }

  }


  def execStatement( sqlStatement: String ): Unit = {

    val sqlCollection: immutable.Iterable[String] = immutable.Iterable[String]( sqlStatement )


    LOG.debug( "Running statement", sqlStatement )
    // Stream the users into the database as insert statements
    val done: Future[Done] =
      Source( sqlCollection )
        .via(
          Slick.flow( ( next: String ) => sqlu"#$next" )
        )
        .log("nr-of-affected-rows")
        .runWith(Sink.ignore)

    Await.result(
      done, Duration.Inf
    )  match {
      case onSuccess => LOG.debug( "Finished ", sqlStatement )
      case onFailure => throw new IllegalStateException(s"Failed to finish: ${sqlStatement}")
    }


  }


  private def mapResultSetToDBType( dbColumnType: DBColumnType[_], positionedResult: PositionedResult ): Any = {
    dbColumnType.typePattern match {
      case v: DBTypeInteger => positionedResult.nextInt()
      case v: DBTypeString => positionedResult.nextString()
      case v: DBTypeFloat => positionedResult.nextFloat()
      case v: DBTypeBoolean => positionedResult.nextBoolean()
      case v: DBTypeLong => positionedResult.nextLong()
      case v: DBTypeDate => positionedResult.nextDate()
      case v: DBTypeBigint => positionedResult.nextBigDecimal()
      case _ => throw new IllegalAccessError(s"Value ${dbColumnType} has no primitive data type match!")
    }
  }


  private def getCastedValueAt( positionedResult: PositionedResult, classFactory: CaseClassFactory[_] ): Any = {

    val tryCast = Try(
      classFactory.orderedFieldDataTypes(positionedResult.currentPos) match {
        case SupportedCaseTypes.INT => positionedResult.nextInt()
        case SupportedCaseTypes.STRING => positionedResult.nextString()
        case SupportedCaseTypes.FLOAT => positionedResult.nextFloat()
        case SupportedCaseTypes.DOUBLE  => positionedResult.nextDouble()
        case SupportedCaseTypes.BOOLEAN => positionedResult.nextBoolean()
        case SupportedCaseTypes.LONG => positionedResult.nextLong()
        case _ => throw new IllegalAccessError(s"Value ${positionedResult.toString()} has no primitive data type match!")
      }
    )

    tryCast match {
      case Success( value ) => value
      case Failure( error ) => {
        LOG.error( "Failed to cast value", s"Converting ${positionedResult.toString()} at position ${positionedResult} using ${classFactory.orderedFieldDataTypes.mkString("|")}" )
        throw error
      }
    }
  }

}
