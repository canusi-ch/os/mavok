package org.mavok.api.sql

import org.mavok.LOG
import org.mavok.api.table.AbstractTable
import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.SQL
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.common.ScopedGraph
import org.mavok.shapeLibrary.rdms.ddl.alterTable.{AlterTablePrimaryKeyCreate, AlterTablePrimaryKeyDrop}
import org.mavok.shapeLibrary.rdms.ddl.{Create, Drop, ImportFile, Truncate}

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class DDLBuilder( table: AbstractTable ) {

  LOG.debug( "DDL Started", "Parameters", table )

  val statements: ListBuffer[ScopedGraph] = ListBuffer[ScopedGraph]()


  def create: DDLBuilder = {
    statements += {
      Create( table.asShape )
    }

    this
  }



  def drop: DDLBuilder = {
    statements += {
      Drop( table.asShape  )
    }

    this
  }



  def importFile( _filePath: String, _delimiter: String ): DDLBuilder = {
    val shape = table.asShape
    statements += {
      ImportFile( _filePath, _delimiter, table.asShape)
    }
    this
  }


  def truncate: DDLBuilder = {
    statements += {
      Truncate( table.asShape   )
    }
    this
  }


  def createPK(): DDLBuilder = {
    statements += {
      AlterTablePrimaryKeyCreate( table, Scope() )
    }
    this
  }

  def dropPK(): DDLBuilder = {
    statements += {
      AlterTablePrimaryKeyDrop( table, Scope() )
    }
    this
  }


  def buildSQLBlocks( connectionType: ConnectionType.Value ): Seq[SQL] = {
    val sequences = ListBuffer[SQL]()
    statements.foreach( statement => sequences ++= statement.toSQL( connectionType  ) )
    sequences
  }


  def build( connectionType: ConnectionType.Value ): String = {
    buildSQLBlocks( connectionType ).map( _.value ).mkString
  }



}

object DDLBuilder {
  def apply(table: AbstractTable ): DDLBuilder = new DDLBuilder(table)
}
