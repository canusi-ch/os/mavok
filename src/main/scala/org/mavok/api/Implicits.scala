package org.mavok.api

import org.mavok.api.column.{ColumnFactory, ColumnValue}
import org.mavok.datatype.dbtype.STRING

/*
 * Copyright: blacktest -- created 17.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object Implicits{


  implicit def convertStringToColumn( stringValue: String ): ColumnValue[STRING] = {
    // todo this is dangerous
    ColumnFactory.buildColumnValue( ColumnFactory.getDefaultDBType,
      "'" + stringValue + "'" )
  }

  implicit def convertStringToColumn( stringValue: String, noQuotes: Boolean ): ColumnValue[STRING] = {
    // todo this is dangerous
    ColumnFactory.buildColumnValue( ColumnFactory.getDefaultDBType,
      stringValue )
  }


}

