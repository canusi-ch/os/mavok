package org.mavok.api.datatype

object Date {

  def currentEpoch: Date = Date( System.currentTimeMillis )

}


case class Date( epochMillis: Long ) extends AnyVal
