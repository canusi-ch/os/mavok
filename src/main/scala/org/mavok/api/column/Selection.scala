package org.mavok.api.column

import org.mavok.common.tools.Memoize

import scala.collection.mutable.ListBuffer

@Deprecated
 class Selection[+T <: Any]( listElements:  List[T] ) {

   private val memoize = Memoize[InternalObject[_], InternalObject[_] ]( ( internalObject: InternalObject[_] ) => { internalObject } )

  def ::[ S >: T]( that: S ): Selection[S] = {
    memoize.apply( new InternalObject[S] )
    val internalObject: InternalObject[S] = memoize.get().asInstanceOf[InternalObject[S]]
    internalObject.listBuffer += that
    this
  }

  def getElements: List[T] = memoize.get().asInstanceOf[InternalObject[T]].listBuffer.toList


   class InternalObject[ S >: T ]{
     val listBuffer = ListBuffer[S]()
   }


}

object Selection {

  def apply[T](elements: T*): Selection[T] = {
    new Selection[T](elements.toList){}
  }



  def apply[T](elements: Seq[T])(implicit dummyImplicit: DummyImplicit ): Selection[T] = {
    new Selection[T]( elements.toList){}
  }


}
