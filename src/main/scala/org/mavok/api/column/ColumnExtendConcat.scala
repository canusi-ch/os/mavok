package org.mavok.api.column

import org.mavok.api.table.DatasetReadable
import org.mavok.datatype.dbtype.{DBColumnType, DataType, STRING}
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.extendedtypes.ColumnExtendConcatShape

/*
 * Copyright: blacktest -- created 24.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ColumnExtendConcat[T <: DataType ](val leftBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]
                                                    , rightBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]] )
  extends AbstractColumnExtender[STRING](  rightBaseColumn, DBColumnType.VARCHAR( leftBaseColumn.dbType.typePattern.width + rightBaseColumn.dbType.typePattern.width )  ){

  override def spawnReferenceShape: ColumnGraph = {
    new ColumnExtendConcatShape( this )
  }
}


object ColumnExtendConcat{
  def apply[T <: DataType ](leftBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]], rightBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]): ColumnExtendConcat[T]
  = { new ColumnExtendConcat( leftBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]], rightBaseColumn: AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]  ) }
}
