package org.mavok.api.column

import org.mavok.api.table.{DatasetPersistent, DatasetReadable}
import org.mavok.common.classvalue.Name
import org.mavok.datatype.dbtype.{DBColumnType, DataType}
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.common.ColumnReference

import scala.collection.mutable
/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

object ColumnOption extends Enumeration {

  val PRIMARY_KEY = Value( "PRIMARY_KEY")
  val NOT_NULL = Value( "NOT_NULL" )
  val UNIQUE = Value( "UNIQUE")

}


class Column[T <: DataType, Source <: DatasetReadable[_] ](val source: DatasetPersistent, val name: Name, dbType: DBColumnType[T]  )
  extends AbstractColumn[T, Source]( name.name, dbType  )  {


  def <<[S <: AbstractColumn[_, _]](that: S): ( Column[_, _], S ) = { ( this -> that ) }

  import ColumnOption._
  private lazy val mapOptions: mutable.Map[ColumnOption.Value, Boolean] = mutable.Map[ColumnOption.Value, Boolean]( ColumnOption.PRIMARY_KEY -> false, ColumnOption.NOT_NULL -> false, ColumnOption.UNIQUE -> false)


  def asPK : this.type = { mapOptions( PRIMARY_KEY ) = true; this  }
  def asUnique: this.type = { mapOptions( UNIQUE ) = true; this  }
  def asNotNull: this.type = { mapOptions( NOT_NULL ) = true; this  }

  def setPK( isPK: Boolean = true )  = { mapOptions( PRIMARY_KEY ) = true  }
  def setUnique( isUnique: Boolean = true ) = { mapOptions( UNIQUE ) = true  }
  def setNotNull( isNullable: Boolean = true ) = { mapOptions( NOT_NULL ) = true  }

  def isPK: Boolean = mapOptions( PRIMARY_KEY )
  def isUnique: Boolean = mapOptions( UNIQUE )
  def isNullable: Boolean = mapOptions( NOT_NULL )

  def getOptions = mapOptions.toMap


  def spawnReferenceShape: ColumnGraph = new ColumnReference( this )

  protected val hashCodeString: String = source + name.name + dbType.toString

}

