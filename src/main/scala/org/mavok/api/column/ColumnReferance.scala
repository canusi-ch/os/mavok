package org.mavok.api.column

import org.mavok.api.table.QueryModelGeneric
import org.mavok.common.classvalue.Name
import org.mavok.datatype.dbtype.{DBColumnType, DataType}
import org.mavok.shapeLibrary.rdms.column.ColumnGraph
import org.mavok.shapeLibrary.rdms.column.common.ColumnReference
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape

/*
 * Copyright: blacktest -- created 01.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




class ColumnReferance[T <: DataType](source: ColumnContainerGraphShape, val name: Name, dbType: DBColumnType[T]  )
  extends AbstractColumn[T, QueryModelGeneric ]( name.name, dbType  ){

  def spawnReferenceShape: ColumnGraph = new ColumnReference( this )

  protected val hashCodeString: String = source + name.name + dbType.toString
}


