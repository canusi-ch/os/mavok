package org.mavok.api.table

import org.mavok.api.column.AbstractColumn
import org.mavok.datatype.dbtype.DataType
import org.mavok.workflowEngine.QueryFlow

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


trait QueryModelGeneric extends DatasetReadable[AbstractColumn[_, _]] {

  private val listColumnRefs = ListBuffer[AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]]()
  override def getColumns: Vector[AbstractColumn[_ <: DataType, _ <: DatasetReadable[_]]] = listColumnRefs.toVector


  def tableQuery: QueryFlow[_]

  protected def columnRef[T <: DataType](column: AbstractColumn[T,_ <: DatasetReadable[_]] ): AbstractColumn[T, _ <: DatasetReadable[_] ] = {
    listColumnRefs += column
    column
  }

}
