package org.mavok.api.table

import org.mavok.controller.ReadableTableController
import org.mavok.datatype.Avro2DBColumnMapping
import org.mavok.jdbc.JDBCPlugin
import org.mavok.meta._

/*
 * Copyright: blacktest -- created 24.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object TableGenericFactory {


  def apply( _tablePath: TablePath, jdbcPlugin: JDBCPlugin ): TableModelGeneric = {

    val metaTableColumns: ReadableTableController[CaseClassMetaTableColumns, MetaTableColumnInterface]
    = MetaTableColumns( jdbcPlugin )


    val metaResults: Seq[CaseClassMetaTableColumns]
    = metaTableColumns.filter( _.colTablePath :== _tablePath.getTablePath )

    require( metaResults.nonEmpty, s"No results found for ${_tablePath.getTablePath}")
    val converter = MapperMetaEntry2DBColumnType( jdbcPlugin.getConnectionType )


    val generatedTable: TableModelGeneric = new TableModelGeneric( _tablePath, jdbcPlugin ){
      override val tablePath: HasTablePath = _tablePath

      metaResults.foreach{
        metaSet => { column( metaSet.columnName,  converter.convert( metaSet ) )}
      }
    }

    syncPKConstraintsWithSource( generatedTable, jdbcPlugin )


    generatedTable
  }


  private def syncPKConstraintsWithSource( table: TableModelGeneric, jdbcPlugin: JDBCPlugin ): Unit = {
    val metaTableColumns: ReadableTableController[CaseClassMetaTableColumnConstraints, MetaTableColumnConstraintsInterface]
    = MetaTableColumnContraints( jdbcPlugin )

    val metaResults: Seq[CaseClassMetaTableColumnConstraints]
    = metaTableColumns.filter( _.colTablePath :== table.tablePath.getTablePath )

    metaResults.foreach{

      nextRecord => if( nextRecord.columnConstraintType == "PRIMARY_KEY" ){
        // todo this is hackish. Add some checks ( counts etc )
        table.getColumns.filter( _.name.name.toLowerCase() == nextRecord.columnName.toLowerCase() ).foreach{
            _.asPK
          }

      }
    }

  }


  def fromAvro(avroRecordSchema: Avro2DBColumnMapping, tablePath: TablePath, JDBCPlugin: JDBCPlugin ): TableModelGeneric = {
    require( avroRecordSchema!= null, "Input cant be null" )
    new  TableModelGeneric(  tablePath, JDBCPlugin) {
      for( i <- avroRecordSchema.fieldNames zip avroRecordSchema.fieldTypes ) {
        column( i._1.name, i._2 )
      }
    }
  }

}

