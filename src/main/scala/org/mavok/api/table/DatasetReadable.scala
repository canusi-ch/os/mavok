package org.mavok.api.table

import org.mavok.api.column.AbstractColumn
import org.mavok.shapeLibrary.shapeResource.DatasetContainer

/*
 * Copyright: blacktest -- created 05.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */





trait DatasetReadable[ +T <: AbstractColumn[_, _]] {

  def getColumns: Vector[T]

  lazy val asShape: DatasetContainer[T] = { new DatasetContainer[T]( this  ) }

}
