package org.mavok.api.table

import org.mavok.LOG
import org.mavok.api.column.{Column, ColumnFactory}
import org.mavok.common.classvalue.Name
import org.mavok.datatype.dbtype.{DBColumnType, DataType}

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
import scala.reflect.runtime.universe._


trait AbstractTable extends DatasetPersistent {


  // todo dbcolumn type, move this out to builder pattern? needs rethink
  protected[table] def column[T <: DataType](name: String, dbType: DBColumnType[T] = null )( implicit m: TypeTag[T] ): Column[T, _ <: this.type] = {
    val column = ColumnFactory.buildColumn( if( dbType == null )  ColumnFactory.getDefaultDBType else dbType , this, Name( name ) )
    registerColumn(  column )
  }

  // todo dbcolumn type, move this out to builder pattern? needs rethink
  protected[table] def registerColumn[T <: DataType]( column: Column[T, _ <: this.type] ): Column[T, _ <: this.type] = {
    LOG.debug( s"Column Constructed in ${tablePath}", s"${column.name.name}: ${column.dbType.toDDL}")
    listColumnHeaders += column
    column
  }



}


