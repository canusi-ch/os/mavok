package org.mavok.workflowEngine

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetReadable
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.dml.JoinSegment
import org.mavok.shapeLibrary.rdms.keywords.ShardJoinType


/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class JoinFlow[ T <: DatasetReadable[_], S <: DatasetReadable[AbstractColumn[_, _]]](queryFlow: QueryFlow[T], table: S, joinType: ShardJoinType ) extends Workflow[JoinSegment] {



  private var comparisonGraph: ColumnCompositeComparisonFlow = _



  def on( comparison: ColumnCompositeComparisonFlow  ): QueryFlow[T] = {
    comparisonGraph = comparison
    queryFlow.listJoins += this
    queryFlow
  }



  override def assembleGraph(scope: Scope): JoinSegment = {
    JoinSegment( table, joinType, comparisonGraph, scope )
  }
}

object JoinFlow {
  def apply[ T <: DatasetReadable[_], S <: DatasetReadable[AbstractColumn[_,_]]](queryFlow: QueryFlow[T], table: S, joinType: ShardJoinType): JoinFlow[T,S] = new JoinFlow[T,S](queryFlow, table, joinType)
}







