package org.mavok.workflowEngine

import org.mavok.api.column.{AbstractColumn, Column}
import org.mavok.api.table.DatasetReadable
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.dml.Insert
import org.mavok.shapeLibrary.shapeResource.ColumnContainerGraphShape



/*
 * You deliver the impossible
 * Copyright: blacktest -- created 25.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class InsertFlow[Table <: DatasetReadable[_]](table: Table, source: ColumnContainerGraphShape) extends Workflow[Insert]  {

  private var insertGraph: Insert = _

  def insertMap( mappings: (Column[_, Table], AbstractColumn[_, _])*  ): InsertFlow[Table] = {

    val columnMap = mappings
    require( columnMap.nonEmpty, "Columns required for mapping")

    val queryColumns = columnMap.map( _._1 )

    if( !source.columns.equals( queryColumns ) ){
      throw new IllegalArgumentException( s"${source.columns.toString} != ${queryColumns.toString}")
    }

    insertGraph = Insert( source.graphScope, source, columnMap.map( _._1 )  )
    this
  }


  def insertBasic(  columns: Column[_, _]*): InsertFlow[Table] = {
    require( columns.nonEmpty, "Columns required for mapping")
    insertGraph = Insert( source.graphScope, source, columns  )
    this
  }

  def on( columnCompositeComparisonFlow: ColumnCompositeComparisonFlow  ): InsertFlow[Table] = {

    this
  }

  override def assembleGraph( scope: Scope = Scope()    ): Insert = {
    insertGraph
  }
}





object InsertFlow {

  def apply[Table <: DatasetReadable[_]](table: Table, source: ColumnContainerGraphShape ): InsertFlow[Table] = new InsertFlow( table, source )
}
