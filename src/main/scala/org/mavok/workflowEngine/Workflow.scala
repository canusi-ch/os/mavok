package org.mavok.workflowEngine

import org.mavok.common.ConnectionType
import org.mavok.common.classvalue.SQL
import org.mavok.scope.Scope
import org.mavok.shapeLibrary.rdms.common.ScopedGraph
import org.mavok.shapeLibrary.rdms.properties.IsOwnerOfResource
import org.mavok.workflowEngine.contextOptions.ContextualOption

/*
 * Copyright: blacktest -- created 20.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



trait Workflow[T <: ScopedGraph] extends IsOwnerOfResource {


// todo remove the scope = null with a scope that is part of the workflow ( but needs a bigger refactor )
  def assembleGraph( scope: Scope = Scope() ): T


  def buildSQLBlocks( connectionType: ConnectionType.Value ): Seq[SQL] = {
    val graph = assembleGraph()

    this.getOwnedResources.foreach{
      graph.addOwnedResource( _ )
    }

    graph.toSQL( connectionType )
  }

  def build( connectionType: ConnectionType.Value  ): String = {
    buildSQLBlocks( connectionType ).map( _.value ).mkString
  }

  def addRuntimeOption[O <:  ConnectionType.Value](contextOptions: ContextualOption[O]* ): this.type = {

    this
  }

}

