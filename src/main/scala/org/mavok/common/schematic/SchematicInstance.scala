package org.mavok.common.schematic

/*
 * Copyright: blacktest -- created 02.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




case class SchematicInstance( values: Vector[ ( SchematicTemplate[_], Seq[Any] ) ] ) {

  // ---------------------  Public Variables

  import scala.reflect.{ClassTag, _}

  def get[T: ClassTag]: Seq[T] = {

    val clazz = classTag[T].runtimeClass


    val filtered = values.filter( storedClass =>{
      storedClass._1.classType.isAssignableFrom( clazz )
    } )

    val mapped = filtered.map( _._2 ).head.map( _.asInstanceOf[T])

    mapped.length match {
      case 0 => throw new NoSuchElementException( clazz.toString )
      case _ => Seq() ++ mapped
    }
  }

  def flatten: Seq[Any] = {
    values.flatMap( _._2 )
  }

}
