package org.mavok.common.schematic

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-23 
 */

object Schematic extends Enumeration {


  def apply(): Schematic = {
    new Schematic()
  }


  val ONE_OR_MORE, ONE, TWO, ONE_OR_TWO, ZERO_OR_ONE, ZERO_OR_MORE = Value

  def isValidCount( count: Int, schematicCount: Value ): Boolean = {
    schematicCount match {
      case ONE_OR_MORE => if( count > 0 ) true else false
      case ONE => if( count == 1 ) true else false
      case TWO => if( count == 2 ) true else false
      case ONE_OR_TWO => if( count == 2  || count == 1 ) true else false
      case ZERO_OR_ONE => if( count == 0 || count == 1 ) true else false
      case ZERO_OR_MORE => if( count >= 0 ) true else false
    }
  }

}

import org.mavok.common.classvalue.SortedObject

import scala.collection.mutable.ListBuffer
import scala.reflect.{ClassTag, _}


case class SchematicTemplate[T](count: Schematic.Value, classType: Class[_], handleMissing: () => T, templateType: TemplateType.Value )

class Schematic {

  private var sortOrder = 0

  val schematicList: ListBuffer[SortedObject[SchematicTemplate[_]]] = ListBuffer[SortedObject[SchematicTemplate[_]]]()

  def withFuture[T: ClassTag](count: Schematic.Value, handleMissing: () => T ): Schematic = {

    val clazz: Class[_] = classTag[T].runtimeClass

    if( !schematicList.exists( _.obj.classType == clazz) ) {
      sortOrder += 1
      schematicList += SortedObject(SchematicTemplate(count, clazz, handleMissing, TemplateType.unknown ), sortOrder)
    } else {
      LoggerSchematic.error( "Illegal Constructor Argument", s"${clazz.toString()} already exists. Full schematicList: ${schematicList.mkString("|")}"
        , throw new IllegalArgumentException( s"Cannot add the same class type 2x to the schematic!") )
    }

    this
  }

  def withKnown[T: ClassTag](count: Schematic.Value, value: T ): Schematic = {
    val clazz: Class[_] = classTag[T].runtimeClass
    sortOrder += 1
    schematicList += SortedObject(SchematicTemplate(count, clazz, () => value, TemplateType.known ), sortOrder)
    this
  }

}



