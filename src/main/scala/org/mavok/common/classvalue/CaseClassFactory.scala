package org.mavok.common.classvalue

import java.lang.reflect.Constructor

import scala.reflect._
import scala.reflect.runtime.universe._
import scala.util.{Failure, Success, Try}


object CaseClassFactory {



  def apply[T <: Product ]()(implicit tag: TypeTag[T]): CaseClassFactory[T] = {

    val productConstructor: Constructor[_] = typeToClassTag.runtimeClass.getConstructors.head
    val dataTypes = getFieldTypes()


    new CaseClassFactory[T]() {
      override protected val constructor: Constructor[_] = productConstructor
      override val orderedFieldDataTypes: Vector[Any] = dataTypes

    }
  }


  private def typeToClassTag[S <: Product, T <: Product  ]()(implicit tag: TypeTag[T]): ClassTag[T] = {
    ClassTag[T]( typeTag[T].mirror.runtimeClass( typeTag[T].tpe ) )
  }



  private def getFieldTypes[T <: Product  ]()(implicit tag: TypeTag[T]): Vector[SupportedCaseTypes.Value] = {

    val allAccessors = typeOf[T].decls.collect { case meth: MethodSymbol if meth.isCaseAccessor => meth }
    val fieldTypes = allAccessors map { sym => ( sym.name.toString -> sym.asMethod ) }



    fieldTypes
      .map( reflectedType => {

        val tag = reflectedType._2.returnType

        tag match {
          case int if int =:= typeOf[Int] => SupportedCaseTypes.INT
          case double if double =:= typeOf[Double] => SupportedCaseTypes.DOUBLE
          case float if float =:= typeOf[Float] => SupportedCaseTypes.FLOAT
          case long if long =:= typeOf[Long] => SupportedCaseTypes.LONG
          case string if string =:= typeOf[String] => SupportedCaseTypes.STRING
          case boolean if boolean =:= typeOf[Boolean] => SupportedCaseTypes.BOOLEAN
          //case enum if enum =:= typeOf[Enumeration#Value] => SupportedCaseTypes.ENUM
          case _ => throw new IllegalArgumentException(s"Invalid column '${reflectedType._1}' with type ${reflectedType._2}. Must be primitive ( Int, String etc )!")
        }
      }
      ).toVector

  }


  object SupportedCaseTypes extends Enumeration {

    val INT, DOUBLE, FLOAT, LONG, STRING, BOOLEAN = Value

  }


}


abstract class CaseClassFactory[T <: Product ]() {

  val orderedFieldDataTypes: Vector[Any]
  protected val constructor: Constructor[_]


  def newInstance( values: Array[AnyRef] ): T = {

    val tryBuilder = Try( constructor.newInstance(values: _*).asInstanceOf[T] )

    tryBuilder match {
      case Success( v ) => v
      case Failure(exception) =>
        throw new IllegalArgumentException(
        s"Cant build ${constructor.getClass} using parameters ${values.mkString("\n")}. " +
        s"Error throw ${exception.getMessage + "|" + exception.getCause + "|" + exception.getStackTrace }" )
    }

  }


  def convertClassToArray(): Array[AnyRef]  = {

    Array[AnyRef]()
  }





}


