package org.mavok.common.classvalue

/*
 * Copyright: blacktest -- created 23.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


case class SchemaName( schemaName: String ) extends AnyVal
case class TableName( tableName: String ) extends AnyVal
case class TableAlias(tableAlias: String ) extends AnyVal

case class ColumnName( columnName: String ) extends AnyVal
case class ColumnAlias( columnName: String ) extends AnyVal


