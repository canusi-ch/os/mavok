package org.mavok.common.classvalue

import org.mavok.common.tools.{BasicCounter, Counter}

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 12.07.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

class SortedObjectList[T]  extends Counter {

  private val list: ListBuffer[SortedObject[T]] = ListBuffer[SortedObject[T]]()

  def add( elem: T ): Unit = {
    require( !contains( elem ))
    list += SortedObject( elem, next.toInt )
  }

  def contains(elem: T ): Boolean = {
    list.exists( _.obj == elem )
  }

  def getSorted: ListBuffer[T] = {
    list.sortBy( _.sortOrder ).map( _.obj )
  }

  def get: ListBuffer[T] = {
    list .map( _.obj )
  }


}


case class SortedObject[T]( obj: T, sortOrder: Int )

case class Name( name: String ) extends AnyVal