package org.mavok.common.tools

/*
 * Copyright: blacktest -- created 03.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



trait ClassUUID {
  val uuid: Long = SingletonCounter.next

  def equalsUUID( that: ClassUUID ): Boolean = this.uuid == that.uuid
}
