package org.mavok.common.tools

import com.typesafe.scalalogging.Logger

/*
 * Copyright: blacktest -- created 07.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */




class BasicLogger( name: String ) {


  /** loads from the resources/application.conf
    * Example is below
    *
    * akka {
    *
    * # options: OFF, ERROR, WARNING, INFO, DEBUG
    * loglevel = "DEBUG"
    *
    * }
    */

  private val LOGGER = Logger( name )


  def info( message: String, description: String ): Unit = {
    LOGGER.info( s"${message}: ${description}")
  }
  def debug( message: String, description: String, args: Any* ): Unit = {
    LOGGER.debug( s"${message}: ${description} --> ${args.mkString(":")}" )
  }

  def warning( message: String, description: String, handler: () => Unit = () => {}   ): Unit = {
    LOGGER.warn( s"${message}: ${description}")
    handler()
  }
  def error( message: String, description: String, errorHandler: () => Unit = () => {}   ): Unit = {
    LOGGER.error( s"${message}: ${description}")
    errorHandler()
  }



}
