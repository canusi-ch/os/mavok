package org.mavok.controller

import org.mavok.api.sql.DDLBuilder
import org.mavok.api.table.AbstractTable
import org.mavok.meta.MetaTableColumns

trait DDLController[Tab <: AbstractTable] extends Controller[Tab] {

  override val table: Tab

  private val self = this

  def tableExists: Boolean = { 0 <  MetaTableColumns.apply( jdbcPlugin ).filter( _.colTablePath :== this.table.tablePath.getTablePath ).size}
  def create: Unit = {
    jdbcPlugin.execStatement( DDLBuilder( table ).create.build( jdbcPlugin.getConnectionType ) )
    if( table.getColumns.exists( _.isPK )) jdbcPlugin.execStatement( DDLBuilder( table ).createPK().build( jdbcPlugin.getConnectionType ) )
  }
  def drop: Unit  = { jdbcPlugin.execStatement( DDLBuilder( table ).drop.build( jdbcPlugin.getConnectionType ) ) }
  def truncate: Unit  = { jdbcPlugin.execStatement( DDLBuilder( table ).truncate.build( jdbcPlugin.getConnectionType ) ) }

  def commit: Unit = jdbcPlugin.execStatement( "commit" )

  def importFile( filePath: String, delimiter: String ): Unit  = {
    jdbcPlugin.execStatement( DDLBuilder( table ).importFile( filePath, delimiter ).build( jdbcPlugin.getConnectionType ) )
  }

}
