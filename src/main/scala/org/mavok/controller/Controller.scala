package org.mavok.controller

import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetReadable
import org.mavok.jdbc.JDBCPlugin

trait Controller[Tab <: DatasetReadable[AbstractColumn[_, _]]]  {

  val jdbcPlugin: JDBCPlugin
  val table: Tab

}
