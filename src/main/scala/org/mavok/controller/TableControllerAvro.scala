package org.mavok.controller

import org.apache.avro.generic.GenericRecord
import org.mavok.api.table.TableModelGeneric
import org.mavok.common.classvalue.SQL
import org.mavok.controller.map.{MapGenericToInsertStatement, MapGenericToUpdateStatement}
import org.mavok.jdbc.JDBCPlugin

import scala.collection.mutable.ListBuffer


/*
 * Copyright: blacktest -- created 20.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */



abstract class TableControllerAvro[Tab <: TableModelGeneric]
  extends DDLController[Tab] {

  override val table: Tab

  private val self = this
  private lazy val genericRecordToInsertMap = MapGenericToInsertStatement( table )
  private lazy val genericRecordToUpdateMap = MapGenericToUpdateStatement( table )

  private lazy val insertBuffer: ListBuffer[SQL] = ListBuffer[SQL]()

  def +=( next: GenericRecord ): Unit = {
    insertBuffer += SQL( genericRecordToInsertMap.prepareStatement( next ).mkString )
  }

  def flush(): Unit = {

    jdbcPlugin.execStatement( insertBuffer.map( _.value ).mkString(";") )

  }

  def update( next: GenericRecord ): Unit = {
    jdbcPlugin.execStatement( genericRecordToUpdateMap.prepareStatement( next ).mkString )
  }

  /* // write this up later
  def filter( assign: Tab => ColumnCompositeComparisonFlow ): MapResultSetToGeneric = {
    jdbcPlugin.execQuery(
      DMLBuilder( table )
        .select( table.getColumns: _* )
        .where( assign( table ) )
        .build( jdbcPlugin.getConnectionType ) )
    new MapResultSetToGeneric( resultSet, table )
  }

  def stream(): MapResultSetToGeneric = {
    val resultSet = jdbcPlugin.execQuery( DMLBuilder( table ).select( table.getColumns: _* ).build( jdbcPlugin.getConnectionType ) )
    new MapResultSetToGeneric( resultSet, table )
  }*/
}

object TableControllerAvro {

  def apply[ T <: TableModelGeneric ](_table: T, _jdbcPlugin: JDBCPlugin ): TableControllerAvro[T] = new TableControllerAvro[T](){
    override val table: T = _table
    override val jdbcPlugin: JDBCPlugin = _jdbcPlugin
  }


}



