package org.mavok.controller

import org.mavok.api.column.AbstractColumn
import org.mavok.api.sql.DMLBuilder
import org.mavok.api.table.{DatasetReadable, QueryModelGeneric}
import org.mavok.jdbc.JDBCPlugin
import org.mavok.workflowEngine.{ColumnCompositeComparisonFlow, QueryFlow, Workflow}

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

import scala.reflect.runtime.universe._
/* todo Maybe tighter restrictions than Product.
 * Meant to let you take in Case Class Tables
 */
private case class Count( count: Long ) extends AnyVal

abstract class ReadableTableController[ V <: Product, T <: DatasetReadable[AbstractColumn[_, _]] ](implicit tag: TypeTag[V])
extends Controller[T]
{


  def count(): Long = {

    import org.mavok.api.Implicits._


    var nextCount: Count = Count(-1)
    var counter = 0

    jdbcPlugin.execQuery(
      DMLBuilder( table ).select( convertStringToColumn( "*", noQuotes =  true  ).count  )
        .build( jdbcPlugin.getConnectionType )
      , ( next: Count ) => { {
        counter += 1
        nextCount = next
      } }
    )

    require( counter == 1 )
    nextCount.count
  }


  def filter(f: T => ColumnCompositeComparisonFlow ): Seq[V] = {
    val listBuffer: ListBuffer[V] = ListBuffer[V]()
    streamOnFilter( f,  ( next:V )  => { listBuffer += next } )
    listBuffer.toSeq
  }

  def streamOnFilter(f: T => ColumnCompositeComparisonFlow, handler: V => Unit  ): Unit = {
    require( table.getColumns.nonEmpty )
    // todo get rid of
    val query = table match {
      case tableQuery: QueryModelGeneric => tableQuery.tableQuery.where( f( table ) ).build( jdbcPlugin.getConnectionType)
      case _ => DMLBuilder( table ).select( table.getColumns: _* ).where( f( table ) ).build( jdbcPlugin.getConnectionType )
    }

    jdbcPlugin.execQuery( query, handler )
  }


  def stream( handler: V => Unit ): Unit = {
    // todo get rid of
    val query = table match {
      case tableQuery: QueryModelGeneric => tableQuery.tableQuery.build( jdbcPlugin.getConnectionType)
      case _ => DMLBuilder( table ).select( table.getColumns: _* ).build( jdbcPlugin.getConnectionType )
    }
    jdbcPlugin.execQuery( query, handler )
  }




  def customQuery( queryFlow: QueryFlow[_], handler: V => Unit ): Unit = {
    jdbcPlugin.execQuery( queryFlow.build( jdbcPlugin.getConnectionType ), handler  )
  }


  protected def mapWorfklow2String( workflow: Workflow[_] ): String =  {
    workflow.build( jdbcPlugin.getConnectionType )
  }

}

object ReadableTableController {


  def apply[ Rec <: Product, Tab <: DatasetReadable[AbstractColumn[_, _]] ](_table: Tab, _jdbcPlugin: JDBCPlugin)(implicit tag: TypeTag[Rec] ): ReadableTableController[Rec, Tab]
  = new ReadableTableController[Rec,Tab]()(tag) {
    override val jdbcPlugin: JDBCPlugin = _jdbcPlugin
    override val table: Tab = _table
  }

}
