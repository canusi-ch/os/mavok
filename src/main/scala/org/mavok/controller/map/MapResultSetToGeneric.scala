package org.mavok.controller.map

import java.sql.ResultSet

import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericRecord}
import org.mavok.api.table.TableModelGeneric
import org.mavok.datatype.dbtype._

class MapResultSetToGeneric( resultSet: ResultSet, table: TableModelGeneric ) extends JDBCOverlay[GenericRecord]( resultSet, table ) {

  private val partiallyCompiledGenericRecord = new GenericData.Record( _: Schema )



  override protected def convertResultRecord(): GenericRecord = {
    val genericRecord: GenericRecord = partiallyCompiledGenericRecord( table.avroSchema.schema )
    for( i <- 0 until resultSet.getMetaData.getColumnCount ){
      val jdbcId = getJDBCIndex( i )
      genericRecord.put(
        table.avroSchema.fieldNames( i ).name
        , mapResultSetToDBType( table.avroSchema.fieldTypes(i), resultSet, jdbcId  ) )
    }
    genericRecord
  }


  private def mapResultSetToDBType( dbColumnType: DBColumnType[_], resultSet: ResultSet, index: Int ): Any = {
    dbColumnType.typePattern match {
      case v: DBTypeInteger => resultSet.getInt(index)
      case v: DBTypeString => resultSet.getString(index)
      case v: DBTypeFloat => resultSet.getFloat(index)
      case v: DBTypeBoolean => resultSet.getBoolean(index)
      case v: DBTypeLong => resultSet.getLong(index)
      case v: DBTypeDate => resultSet.getDate(index)
      case v: DBTypeBigint => resultSet.getBigDecimal(index)
      case _ => throw new IllegalAccessError(s"Value ${dbColumnType} has no primitive data type match!")
    }
  }

}


// zig