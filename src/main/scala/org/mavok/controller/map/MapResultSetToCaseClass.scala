package org.mavok.controller.map

import java.sql.ResultSet

import org.mavok.LOG
import org.mavok.api.column.AbstractColumn
import org.mavok.api.table.DatasetReadable
import org.mavok.common.classvalue.CaseClassFactory
import org.mavok.common.classvalue.CaseClassFactory.SupportedCaseTypes

import scala.util.{Failure, Success, Try}

/*
 * Copyright: blacktest -- created 23.08.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */
import scala.reflect.runtime.universe._

object MapResultSetToCaseClass {

  // ---------------------  Constructors

  def apply[C <: Product ](_resultSet: ResultSet, table: DatasetReadable[AbstractColumn[_, _]] )(implicit tag: TypeTag[C]): MapResultSetToCaseClass[C] = {

    val classFactory = CaseClassFactory[C]()

    require( classFactory.orderedFieldDataTypes.length == _resultSet.getMetaData.getColumnCount
    , throw new IllegalArgumentException( s"" +
        s"ClassFactory Length: ${classFactory.orderedFieldDataTypes.length } != ResultSet Length: ${_resultSet.getMetaData.getColumnCount}"))

    new MapResultSetToCaseClass[C]( _resultSet, table: DatasetReadable[AbstractColumn[_, _]]  ) {
    }
  }

}

class MapResultSetToCaseClass[ C <: Product]( resultSet: ResultSet
                                              , table: DatasetReadable[AbstractColumn[_, _]]  )
                                            (implicit tag: TypeTag[C])
  extends JDBCOverlay[C]( resultSet, table: DatasetReadable[AbstractColumn[_, _]]  ) {

  protected val classFactory: CaseClassFactory[C] = CaseClassFactory[C]()


  override protected def convertResultRecord(): C = {

    val columnCount = resultSet.getMetaData.getColumnCount
    val result = new Array[Any](columnCount)

    for( i <- 0 until columnCount ){
      result(i) = getCastedValueAt(i)
    }

    classFactory.newInstance( result.map( _.asInstanceOf[AnyRef]) )
  }


  private def getCastedValueAt(indexPos: Int ): Any = {

    // todo + 1 is added jdbc uses different indexing system to regular java
    val resultSetIndex = indexPos + 1
    val tryCast = Try(
      classFactory.orderedFieldDataTypes(indexPos) match {
        case SupportedCaseTypes.INT => resultSet.getInt(resultSetIndex)
        case SupportedCaseTypes.STRING => resultSet.getString(resultSetIndex)
        case SupportedCaseTypes.FLOAT => resultSet.getFloat(resultSetIndex)
        case SupportedCaseTypes.DOUBLE  => resultSet.getDouble(resultSetIndex)
        case SupportedCaseTypes.BOOLEAN => resultSet.getBoolean(resultSetIndex)
        case SupportedCaseTypes.LONG => resultSet.getLong(resultSetIndex)
        case _ => throw new IllegalAccessError(s"Value ${classFactory.orderedFieldDataTypes(indexPos)} has no primitive data type match!")

      } )

    tryCast match {
      case Success( value ) => value
      case Failure( error ) => {
        LOG.error( "Failed to cast value", s"Converting ${resultSet.getString(indexPos)} at position ${indexPos} using ${classFactory.orderedFieldDataTypes.mkString("|")}" )
        throw error
      }
    }
  }

}

