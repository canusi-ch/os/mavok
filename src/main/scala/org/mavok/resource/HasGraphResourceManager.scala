package org.mavok.resource

import org.mavok.LOG

import scala.collection.mutable.ListBuffer

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


/**
  * Simple wrapper for resource manager
  */
trait HasGraphResourceManager extends Resource {

  lazy val graphResourceManager: NestedGraphResource = NestedGraphResource()

  override def has(resource: Resource): Boolean = { ( resource equals this ) || graphResourceManager.has( resource )}

}
