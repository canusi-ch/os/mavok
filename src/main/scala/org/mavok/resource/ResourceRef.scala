package org.mavok.resource

import org.mavok.shapeLibrary.rdms.keywords.Alias

/*
 * Copyright: blacktest -- created 05.06.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


class ResourceRef[T <: Resource ]( val resourceTarget: T, val alias: Alias ) extends Resource {

  override def has(resource: Resource): Boolean = {
    ( resourceTarget equals resource ) || ( resource equals this )
  }

}
