package org.mavok.datatype.avro.logicalType

import org.apache.avro.{LogicalType, Schema}

/**
 * Copyright: Canusi
 *
 * @author Benjamin Hargrave
 * @since 2019-11-23 
 */


object LogicalDate {

  private val PRECISION_PROP = "precision"
  private val SCALE_PROP = "scale"

  def apply( _precision: Int, _scale: Int): LogicalDate = new LogicalDate() {

    override val precision: Int = _precision
    override val scale: Int = _scale

  }

}

abstract class LogicalDate extends LogicalType( "test") {

  val precision: Int
  val scale: Int

  override def addToSchema(schema: Schema): Schema = {
    super.addToSchema(schema)
    schema.addProp( LogicalDate.PRECISION_PROP, precision)
    schema.addProp( LogicalDate.SCALE_PROP, scale)
    schema
  }


  override def validate(schema: Schema): Unit = {
    super.validate(schema)

  }

  override def equals(thatDecimal: Any): Boolean = {
    if (this == thatDecimal) return true

    thatDecimal match {
      case decimal: LogicalDecimal => precision == decimal.precision && scale == decimal.scale
      case _ => false
    }

  }

  override def hashCode: Int = {
    "LOGICAL_DECIMAL".hashCode + precision + scale
  }
}

