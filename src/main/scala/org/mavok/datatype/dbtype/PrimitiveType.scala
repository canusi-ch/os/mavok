package org.mavok.datatype.dbtype

/*
 * Copyright: blacktest -- created 08.01.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object PrimitiveType extends Enumeration {

  val STRING: PrimitiveType.Value = Value( 9, "String")
  val NUMBER: PrimitiveType.Value = Value( 6, "Number")
  val DATE: PrimitiveType.Value = Value( 7, "Date")
  val TIMESTAMP: PrimitiveType.Value = Value( 8, "Timestamp")
  val FLOAT: PrimitiveType.Value = Value( 4, "Float")
  val DOUBLE: PrimitiveType.Value = Value( 5, "Double")
  val BIGINT: PrimitiveType.Value = Value( 3, "BigInt")
  val LONG: PrimitiveType.Value = Value( 11, "Long")
  val INTEGER: PrimitiveType.Value = Value( 2, "Int")
  val BOOLEAN: PrimitiveType.Value = Value( 1, "Boolean" )
  val NULL: PrimitiveType.Value = Value( 10, "Null" )

}

trait DataType {
  val primitiveType: PrimitiveType.Value
}

trait NUMBER extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.NUMBER; private val id: Int = 1 }
trait DATE extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.DATE }
trait TIMESTAMP extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.TIMESTAMP }
trait FLOAT extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.FLOAT }
trait DOUBLE extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.DOUBLE }
trait BIGINT extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.BIGINT }
trait LONG extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.LONG; private val id: Int = 2 }
trait INTEGER extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.INTEGER }
trait BOOLEAN extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.BOOLEAN }
trait NULL extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.NULL }
trait STRING extends DataType{ val primitiveType: PrimitiveType.Value = PrimitiveType.STRING; private val id: Int = 0 }