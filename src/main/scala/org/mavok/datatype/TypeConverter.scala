package org.mavok.datatype

import org.apache.avro.LogicalTypes.{Date, Decimal, TimestampMicros, TimestampMillis}
import org.apache.avro.Schema
import org.apache.avro.Schema.Type
import org.mavok.api.column.ColumnFactory
import org.mavok.datatype.avro.logicalType.{AvroLogicalTypeRegister, LogicalVarchar}
import org.mavok.datatype.dbtype._

import scala.util.{Failure, Success, Try}
/*
 * Copyright: blacktest -- created 12.05.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

/*
object RegisteredAvroTypes extends Enumeration {

// todo how to extract unique list of avro types

  val DECIMAL: Value = Value( decimal.getName )
  val TIMESTAMPMICROS: Value = Value( timestampMicros.getName )
  val TIMESTAMPMILLIS: Value = Value( timestampMillis.getName )
  val DATE: Value = Value( date.getName )
  val LOGICALVARCHAR: Value = Value( logicalVarchar )


  private val decimal: Decimal = new Decimal().addToSchema( Schema )
  private val timestampMicros: TimestampMicros = new TimestampMicros()
  private val timestampMillis: TimestampMillis = new TimestampMillis()
  private val date: Date = new Date()
  private val logicalVarchar: String = "LOGICAL_VARCHAR"

}*/


object TypeConverter {

  AvroLogicalTypeRegister.registerLogicalTypes()



  /*

  // todo Needs some adjustment with avro primitive conversion
    def fromAvroRecordToPrimitive[T]( schema: Schema, datum: T ): Any = {
      val logicalType = schema.getLogicalType
      val conversion: Conversion[T] = new Conversion[T](){}
      schema.getType match {
        case RECORD => conversion.toRecord( datum, schema, logicalType)
        case ENUM => conversion.toEnumSymbol(datum, schema, logicalType)
        case ARRAY => conversion.toArray(datum, schema, logicalType)
        case MAP => conversion.toMap(datum, schema, logicalType)
        case FIXED => conversion.toFixed(datum, schema, logicalType)
        case STRING => conversion.toCharSequence(datum, schema, logicalType)
        case BYTES => conversion.toBytes(datum, schema, logicalType)
        case INT => conversion.toInt(datum, schema, logicalType)
        case LONG => conversion.toLong(datum, schema, logicalType)
        case FLOAT => conversion.toFloat(datum, schema, logicalType)
        case DOUBLE => conversion.toDouble(datum, schema, logicalType)
        case BOOLEAN => conversion.toBoolean(datum, schema, logicalType)
      }
    }
*/



  def mapDBColumnType2AvroField(columnType: DBColumnType[_] ): String = {
    columnType.typePattern match {

      case boolean: DBTypeBoolean => """"boolean""""
      case string: DBTypeString => """"string""""

       /*
        """{
        "type": "bytes",
        "logicalType": "logical_varchar",
        "length": """ + { if( string.width == 0 ) 10480000.toString else string.width } + """
      }"""*/
      case integer: DBTypeInteger => """"int""""
      case bigint: DBTypeBigint => """"long""""
      case long: DBTypeLong => """"long""""
      case date: DBTypeDate => """{ "type": ["long", "null"], "logicalType": "date"}"""
      case float: DBTypeFloat => """{
        "type": "bytes",
        "logicalType": "decimal",
        "precision": """ + float.width + """,
        "scale": """ + float.scale + """
      }"""
      case _ => throw new IllegalArgumentException("Data type")
    }
  }


  def mapAvroField2DBColumnType(schema: Schema ): DBColumnType[_ <: DataType] = {
    schema.getType match {
      case Type.RECORD => throw new IllegalArgumentException("RECORD isnt currently supported by Mavok DB Column Types")
      case Type.ENUM => throw new IllegalArgumentException("ENUM isnt currently supported by Mavok DB Column Types. Try casting to LOGICAL_VARCHAR")
      case Type.ARRAY => throw new IllegalArgumentException("ARRAY isnt currently supported by Mavok DB Column Types")
      case Type.MAP => throw new IllegalArgumentException("MAP isnt currently supported by Mavok DB Column Types")
      case Type.STRING => DBColumnType.VARCHAR( 1048000 )
      case Type.FLOAT => throw new IllegalArgumentException("FLOAT isnt currently supported by Mavok DB Column Types. Try using LOGICAL_FLOAT")
      case Type.DOUBLE => throw new IllegalArgumentException("DOUBLE isnt currently supported by Mavok DB Column Types. Try using LOGICAL_DOUBLE")
      case Type.INT => DBColumnType.INTEGER
      case Type.LONG => DBColumnType.LONG
      case Type.BOOLEAN => DBColumnType.BOOLEAN

      case Type.BYTES | Type.FIXED => {
        val getLogType = Try(schema.getLogicalType)
        getLogType match {
          case Failure(exception) => throw new IllegalArgumentException("BYTES/FIXED not supported directly by Mavok DB Column Types. No logical type substitute found")
          case Success(value) => value match {
            case v: Decimal => DBColumnType.DOUBLE(v.getPrecision, v.getScale)
            case v: Date => DBColumnType.DATE
            case v: TimestampMicros => DBColumnType.TIMESTAMP
            case v: TimestampMillis => DBColumnType.TIMESTAMP
            case v: LogicalVarchar => DBColumnType.VARCHAR( v.length )
            case _ => throw new IllegalArgumentException(s"No type found for ${schema.getLogicalType}")
          }
        }
      }

    }
  }


  // todo this will be changed to logical types in immediate future
  def mapPrimitive2DBColumnType(value: Any ): DBColumnType[_ <: DataType] = {
    value match {
      case v: Int => ColumnFactory.getDefaultDBType
      case v: String => ColumnFactory.getDefaultDBType
      case v: Float => ColumnFactory.getDefaultDBType
      case v: Double => ColumnFactory.getDefaultDBType
      case v: Boolean => ColumnFactory.getDefaultDBType
      case v: Long => ColumnFactory.getDefaultDBType
      case _ => throw new IllegalAccessError(s"Value ${value} has no primitive data type match!")
    }
  }

}
