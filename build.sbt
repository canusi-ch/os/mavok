name := "org/mavok"

version := "0.1"

scalaVersion := "2.13.1"

scalaVersion := "2.12.7"


resourceDirectory in Compile := baseDirectory.value / "src"/ "main" / "resources"
resourceDirectory in Test := baseDirectory.value / "src"/ "test" / "resources"

scalaSource in Compile := baseDirectory.value / "src"/ "main" / "scala"
scalaSource in Test := baseDirectory.value / "src"/ "test" / "scala"

// https://mvnrepository.com/artifact/org.json4s/json4s-jackson
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"
libraryDependencies += "org.apache.avro"  % "avro" %  "1.8.2"




// MiniApps -------------------------- Job
lazy val akkaVersion = "2.5.19"
lazy val scalaTestVersion = "3.0.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion,
  "org.scala-graph" %% "graph-core" % "1.13.0" withSources(),
  "com.lightbend.akka" %% "akka-stream-alpakka-slick" % "1.1.2"
)

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3"
  , "org.postgresql" % "postgresql" % "42.2.6"

  // Required for generating the dynamic case classes for the ch.upc.database tables
  ,"org.scala-lang" % "scala-compiler" % "2.12.7"
)

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)


libraryDependencies ++=
  Seq("org.slf4j" % "slf4j-api" % "1.7.5",
    "org.slf4j" % "slf4j-simple" % "1.7.5"
    , "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
    // https://mvnrepository.com/artifact/com.chuusai/shapeless
    // https://mvnrepository.com/artifact/com.chuusai/shapeless
    , "com.chuusai" %% "shapeless" % "2.3.3"
    , "com.typesafe.slick" %% "slick" % "3.3.1"


  )
